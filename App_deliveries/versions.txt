Holmium updates

Holmium v4.1.9 - 16/06/2022
- New integrations:
   + Doxel
- Ppt templates:
   + New:
     · Doxel
   + Updated:
     · Baia

Holmium v4.1.8 - 27/05/2022
- New integrations:
   + MediQuo

Holmium v4.1.7 - 25/05/2022
- New integrations:
   + Nubentos
 - Integrations updated:
   + Ifeel
   + Validated ID

Holmium v4.1.2 - 26/04/2022
- New integrations:
   + Tenzir
   + Liux
   + Ekuore
 - Fixed templates:
   + Circular
   + Liux
   + Ekuore


Holmium v4 - 20/04/2022
- New templates:
   + Triditive
 - Fixed templates:
   + Gelt Argentina
 - Images to named cells:
   + dotGIS


Holmium v4 - 04/04/2022
 - Fixed templates:
   + Agrosingularity
   + Nova

Holmium v4 - 30/03/2022
 - Folders Tool Restructuration.
 - New templates:
   + Baia

Holmium v4 - 24/03/2022
 - Fixed next templates:
   + Gasmobi
   + Iomob
 - Improvement:
   + Slide templates 2 & 4:
     · New function "copy_img_in_center" -> Copy images centered and proportionated
   + Join images horizontally -> function "join_image_h"
