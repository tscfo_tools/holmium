# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 09:40:06 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def nova(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING->"
    hoja2= "KPIs"
    hoja3= "PL"
    hoja4= "CF-C"

    lista=[ # Cover
            [hoja1, 1, 2, 39, 20, "Cover"], # 0
            # KPIs
            [hoja2, 28, 4, 42, 19, "KPI_Metrics"], # 1
            [hoja2, 4, 4, 5, 19, "KPI_title"], # 2
            # PL
            [hoja3, 7, 4, 36, 58, "PL_Sales"], # 3
            [hoja3, 37, 4, 59, 58, "PL_Costs"], # 4
            [hoja3, 61, 4, 66, 58, "PL_Other"], # 5
            [hoja3, 4, 4, 5, 58, "PL_Title"], # 6
            # CF
            [hoja4, 6, 4, 28, 58, "CF"], # 7
            [hoja4, 4, 4, 5, 58, "CF_title"] # 8
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)

    # Join images
    # KPIs
    juntar_imagenes(lista_imagenes2[2],lista_imagenes2[1])
    # PL
    juntar_imagenes(lista_imagenes2[6],lista_imagenes2[3])
    juntar_imagenes(lista_imagenes2[6],lista_imagenes2[4])
    juntar_imagenes(lista_imagenes2[6],lista_imagenes2[5])
    # CF
    juntar_imagenes(lista_imagenes2[8],lista_imagenes2[7])

    #eliminar imágenes auxiliares
    remove(lista_imagenes2[2])
    remove(lista_imagenes2[6])
    remove(lista_imagenes2[8])
    
    lista_slides=[
        [4, lista_imagenes2[0]],
        [1, 'KPIs'],
        [2, lista_imagenes2[1],'KPIs – Metrics'],
        [1,"P&L"],
        [2,lista_imagenes2[3],'P&L - Revenue and Cost of Sales'],
        [2,lista_imagenes2[4],'P&L - Fixed costs'],
        [2,lista_imagenes2[5],'P&L - Other results'],
        [1,'Cash'],
        [2,lista_imagenes2[7],'Cash']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)