# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 10:59:07 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir


#PROBAR
#prints_and_plots(archivo,hoja_resumen,lista_part_prints,carpeta_cliente)
def idoven(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1 = "REPORTING->"
    hoja2 = "KPIs"
    hoja3 = "PL"
    hoja4 = "CF"

    lista=[
            # KPIs
            [hoja2, 4, 4, 5, 58, "KPIs_title"],
            [hoja2, 8, 4, 13, 58, "KPIs_SaaS_1"],
            [hoja2, 50, 4, 54, 58, "KPIs_SaaS_2"],
            [hoja2, 61, 4, 65, 58, "KPIs_Eff"],
            [hoja2, 89, 4, 95, 58, "KPIs_HC"],
            # PLs
            [hoja3, 4, 4, 5, 58, "PL_title"],
            [hoja3, 6, 4, 19, 58, "PL_Sales"],
            [hoja3, 20, 4, 37, 58, "PL_Fixed"],
            [hoja3, 38, 4, 48, 58, "PL_Others"],
            #CF
            [hoja4, 4, 4, 5, 58, "CF_title"],
            [hoja4, 6, 4, 18, 58, "CF_Op"],
            [hoja4, 19, 4, 33, 58, "CF_Fin"],
            # REPORTING
            [hoja1, 5, 2, 19, 16, "Reporting_table"],
            [hoja1, 21, 2, 42, 16, "Reporting_figures"]
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)
    
    # Join tabs with their headers
    ## KPIs
    juntar_imagenes(lista_imagenes2[0], lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[0], lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[0], lista_imagenes2[3])
    juntar_imagenes(lista_imagenes2[0], lista_imagenes2[4])
    ## PL
    juntar_imagenes(lista_imagenes2[5],lista_imagenes2[6])
    juntar_imagenes(lista_imagenes2[5],lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[5],lista_imagenes2[8])
    ## CF
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[10])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[11])

    
    lista_slides=[
        [5, lista_imagenes2[12], 1.13, 4.2, 31.6],
        [5, lista_imagenes2[13], 1.13, 4.2, 31.6],
        [1, 'KPIs'],
        [6, 'KPIs – SaaS Metrics', (lista_imagenes2[1], lista_imagenes2[2]), (1.87, 1.87), (5.38, 11.2), (21, 21)],
        [6, 'KPIs – Efficiency metrics and headcount', (lista_imagenes2[3], lista_imagenes2[4]), (1.87, 1.87), (5.38, 11.2), (21, 21)],
        [1,'P&L'],
        [6, 'P&L - Revenue and Cost of sales', lista_imagenes2[6], 2, 5, 20],
        [6, 'P&L - Fixed costs', lista_imagenes2[7], 2, 5, 20],
        [2, lista_imagenes2[8], 'P&L - Other results', 25],
        [1,'Cash'],
        [2, lista_imagenes2[10], 'Operational Cash', 25],
        [2, lista_imagenes2[11], 'Financial Cash', 25]
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)