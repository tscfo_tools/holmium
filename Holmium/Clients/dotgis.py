# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 17:13:24 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints,crear_ppt, prints_and_plots_cell_names
from os.path import join,abspath
from os import pardir, remove

def dotgis(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    hoja1 = "REPORTING ->"
    hoja2 = "PL"
    hoja3 = "PL-IMAG"
    hoja4 = "PL-Lines"
    hoja5 = "CF"

    lista=[
            [hoja2, "PL_1"], # 0
            [hoja2, "PL_2"], # 1
            [hoja2, "PL_3"], # 2
            [hoja2, "PL_4"], # 3
            [hoja2, "PL_TITULOS"], # 4

            [hoja3, "IMAG_1"],  # 5
            [hoja3, "IMAG_2"], # 6
            [hoja3, "IMAG_3"], # 7
            [hoja3, "IMAG_TITULOS"], # 8
            
            [hoja4, "PL_LINES_1"], # 9
            [hoja4, "PL_LINES_2"], # 10
            [hoja4, "PL_LINES_3"], # 11
            [hoja4, "PL_LINES_TITULOS"], # 12

            [hoja5, "CF_1"], # 13
            [hoja5, "CF_2"], # 14
            [hoja5, "CF_3"], # 15
            [hoja5, "CF_TITULOS"] # 16
        ]
    
    guardar_en = abspath(join(guardar_como, pardir))
    # lista_imagenes = prints(archivo, lista, guardar_en)
    imagenes1, lista_imagenes, lista_imagenes3 = prints_and_plots_cell_names(archivo, hoja1, lista, guardar_en)
    

    juntar_imagenes(lista_imagenes[4],lista_imagenes[0])
    juntar_imagenes(lista_imagenes[4],lista_imagenes[1])
    juntar_imagenes(lista_imagenes[4],lista_imagenes[2])
    juntar_imagenes(lista_imagenes[4],lista_imagenes[3])

    juntar_imagenes(lista_imagenes[8],lista_imagenes[5])
    juntar_imagenes(lista_imagenes[8],lista_imagenes[6])
    juntar_imagenes(lista_imagenes[8],lista_imagenes[7])

    juntar_imagenes(lista_imagenes[12],lista_imagenes[9])
    juntar_imagenes(lista_imagenes[12],lista_imagenes[10])
    juntar_imagenes(lista_imagenes[12],lista_imagenes[11])

    juntar_imagenes(lista_imagenes[16],lista_imagenes[13])
    juntar_imagenes(lista_imagenes[16],lista_imagenes[14])
    juntar_imagenes(lista_imagenes[16],lista_imagenes[15])

    #eliminar imágenes auxiliares
    remove(lista_imagenes[4])
    remove(lista_imagenes[8])
    remove(lista_imagenes[12])
    remove(lista_imagenes[16])
    
    lista_slides=[
        [1,"P&L"],
        [2, lista_imagenes[0], 'P&L - Revenue and Cost of sales'],
        [2, lista_imagenes[1], 'P&L - Fixed costs'],
        [2, lista_imagenes[2], 'P&L - Fixed costs'],
        [2, lista_imagenes[3], 'P&L - Other results'],

        [1,'P&L - Imageryst'],
        [2, lista_imagenes[5], 'P&L - Imageryst'],
        [2, lista_imagenes[6], 'P&L - Imageryst'],
        [2, lista_imagenes[7], 'P&L - Imageryst'],

        [1,'PL-Lines'],
        [2, lista_imagenes[9], 'PL–Lines: Solutions'],
        [2, lista_imagenes[10], 'PL–Lines: Data'],
        [2, lista_imagenes[11], 'PL–Lines: Support'],

        [1,'Cash'],
        [2, lista_imagenes[13], 'Cash – Operational cash flow'],
        [2, lista_imagenes[14], "Cash – Financial cash Flow & CL"], 
        [2, lista_imagenes[15], "Cash position EoP"]
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)