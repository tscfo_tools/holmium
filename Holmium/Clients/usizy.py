# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 17:47:43 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir


#PROBAR
#prints_and_plots(archivo,hoja_resumen,lista_part_prints,carpeta_cliente)
def usizy(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING->"
    hoja2="PL"
    hoja3="CF"
    hoja4="KPIs"
    hoja5="Cohorts"

    lista=[
            [hoja2,4,3,23,58,"PL1"],
            [hoja2,24,3,40,58,"PL2"],
            [hoja2,41,3,50,58,"PL3"],
            [hoja2,4,3,5,58,"PL_titulos"],
            [hoja3,4,3,14,58,"CF1"],
            [hoja4,8,3,26,58,"KPIs1"],
            [hoja4,27,3,38,58,"KPIs2"],
            [hoja5,8,2,33,30,"KPIs3"],
            [hoja4,52,3,57,58,"KPIs4"],
            [hoja4,60,3,83,58,"KPIs5"],
            [hoja4,86,3,92,58,"KPIs6"],
            [hoja4,4,3,5,58,"KPIs_titulos"],
            [hoja1,1,1,23,16,"REPORTING"],
            [hoja3,15,3,27,58,"CF2"],
            [hoja3,4,3,5,58,"CF_titulos"]
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[5])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[6])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[8])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[9])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[10])
    juntar_imagenes(lista_imagenes2[14],lista_imagenes2[13])
    
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[1]
    plot2=lista_imagenes1[2]
    screen2=lista_imagenes2[0]
    screen3=lista_imagenes2[1]
    screen4=lista_imagenes2[2]
    screen5=lista_imagenes2[4]
    screen6=lista_imagenes2[5]
    screen7=lista_imagenes2[6]
    screen8=lista_imagenes2[7]
    screen9=lista_imagenes2[8]
    screen10=lista_imagenes2[9]
    screen11=lista_imagenes2[10]
    screen12=lista_imagenes2[13]
    
    
    lista_slides=[
        [5,(screen1,plot1,plot2),(5.53,2.53,16.93),(0,9.52,9.52),(22.6,14.34,14.34)],
        [1,'KPIs'],
        [2,screen6,'KPIs – SAAS Metrics',23.49],
        [2,screen7,'KPIs – SAAS Metrics',27.69],
        [2,screen8,'KPIs – Cohorts',27.50],
        [2,screen9,'KPIs – MRR by Region',30.35],
        [2,screen10,'KPIs – Efficiency',22.07],
        [2,screen11,'KPIs – Headcount',27.24],
        [1,"P&L"],
        [2,screen2,'P&L - Revenue and Cost of sales',25.74],
        [2,screen3,'P&L - Fixed costs',27.04],
        [2,screen4,'P&L - Other results',30.52],
        [1,'Cash'],
        [2,screen5,'Cash – Operational cash flow',32.01],
        [2,screen12,'Cash – Financial cash Flow & Cash Position',30.76]
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)