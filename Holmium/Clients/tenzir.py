from Functions.funciones import juntar_imagenes, join_image_h, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def tenzir(archivo, guardar_como, template, mes):
    
    # WorkSheets of interest
    hoja1= "REPORTING->"
    hoja2= "KPIs"
    hoja3= "PL"
    hoja4= "CF"

    lista=[ # KPIs
            [hoja2, 8, 4, 26, 70, "KPI_SAAS_1"], # 0
            [hoja2, 31, 4, 53, 70, "KPI_SAAS_2"], # 1
            [hoja2, 90, 4, 100, 70, "KPI_HC"], # 2
            [hoja2, 4, 4, 5, 70, "KPI_title"], # 3
            # P&L
            [hoja3, 6, 4, 17, 70, "Fin_Sum"], # 4
            [hoja3, 18, 4, 35, 70, "PL_fix"], # 5
            [hoja3, 36, 4, 47, 70, "PL_others"], # 6
            [hoja3, 4, 4, 5, 70, "PL_title"], # 7
            #CFs
            [hoja4, 6, 4, 31, 70, "CF"], # 8
            [hoja4, 4, 4, 5, 70, "CF_title"] # 9
        ]
    
    guardar_en = abspath(join(guardar_como, pardir))
    lista_imagenes1, lista_imagenes2, lista_imagenes_aux = prints_and_plots(archivo, hoja1, lista, guardar_en)
    
    # Join images
    ## KPIs
    juntar_imagenes(lista_imagenes2[3], lista_imagenes2[0])
    juntar_imagenes(lista_imagenes2[3], lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3], lista_imagenes2[2])
    # P&L
    juntar_imagenes(lista_imagenes2[7], lista_imagenes2[4])
    juntar_imagenes(lista_imagenes2[7], lista_imagenes2[5])
    juntar_imagenes(lista_imagenes2[7], lista_imagenes2[6])
    ## CF
    juntar_imagenes(lista_imagenes2[9], lista_imagenes2[8])

    #eliminar imágenes auxiliares (headers)
    remove(lista_imagenes2[3])
    remove(lista_imagenes2[7])
    remove(lista_imagenes2[9])
    
    # ppt layout    
    lista_slides=[
        # Financial Summary
        [1,'Financial Summary'],
        [2, lista_imagenes2[4], 'Financial Summary'],
        # P&L
        [1,'P&L'],
        [2, lista_imagenes2[5], 'P&L'],
        [2, lista_imagenes2[6], 'P&L'],
        # Cash Flow
        [1, 'Cash Flow'],
        [2, lista_imagenes2[8],'Cash'],
        # P&L
        [1,'KPIs'],
        [2, lista_imagenes2[0], 'KPIs - SAAS METRICS'],
        [2, lista_imagenes2[1], 'KPIs - SAAS METRICS'],
        [2, lista_imagenes2[2], 'KPIs - HEADCOUNT']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)