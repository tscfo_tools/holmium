# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 09:34:47 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir,remove


#PROBAR
#prints_and_plots(archivo,hoja_resumen,lista_part_prints,carpeta_cliente)
def nora(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="Cover Reporting->"
    hoja2="PL"
    hoja3="CF"
    hoja4="KPIs"

    lista=[
            [hoja2,1,3,39,58,"PL1"],
            [hoja2,40,3,57,58,"PL2"],
            [hoja2,58,3,68,58,"PL3"],
            [hoja2,1,3,5,58,"PL_titulos"],
            [hoja3,1,3,27,58,"CF1"],
            [hoja4,8,3,14,19,"KPIs1.1"],
            [hoja4,34,3,36,19,"KPIs1.2"],
            [hoja4,74,3,75,19,"KPIs1.3"],
            [hoja4,96,3,97,19,"KPIs1.4"],
            [hoja4,99,3,101,19,"KPIs1"],
            [hoja4,104,3,125,19,"KPIs2"],
            [hoja4,128,3,136,19,"KPIs3"],
            [hoja4,1,3,5,19,"KPIs_titulos"]
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[5],lista_imagenes2[6])
    juntar_imagenes(lista_imagenes2[6],lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[7],lista_imagenes2[8])
    juntar_imagenes(lista_imagenes2[8],lista_imagenes2[9])
    juntar_imagenes(lista_imagenes2[12],lista_imagenes2[9])
    juntar_imagenes(lista_imagenes2[12],lista_imagenes2[10])
    juntar_imagenes(lista_imagenes2[12],lista_imagenes2[11])
    
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[1]
    plot2=lista_imagenes1[2]
    screen2=lista_imagenes2[0]
    screen3=lista_imagenes2[1]
    screen4=lista_imagenes2[2]
    screen5=lista_imagenes2[4]
    screen6=lista_imagenes2[9]
    screen7=lista_imagenes2[10]
    screen8=lista_imagenes2[11]
    
    #Eliminar imágenes auxiliares
    remove(lista_imagenes2[5])
    remove(lista_imagenes2[6])
    remove(lista_imagenes2[7])
    remove(lista_imagenes2[8])

    
    lista_slides=[
        [3,(screen1,plot1,plot2)],
        [1,'KPIs'],
        [2,screen6,'KPIs – Metrics'],
        [2,screen7,'KPIs – Efficiency Web'],
        [2,screen8,'KPIs – Headcount'],
        [1,"P&L"],
        [2,screen2,'P&L - Revenue and Cost of sales'],
        [2,screen3,'P&L - Fixed costs'],
        [2,screen4,'P&L - Other results'],
        [1,'Cash'],
        [2,screen5,'Cash']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)