# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 10:33:06 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def build(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING->"
    hoja2="PL"
    hoja3="CF"
    hoja4="KPIs"

    lista=[
            [hoja2,4,4,17,58,"PL1"],
            [hoja2,19,4,33,58,"PL2"],
            [hoja2,35,4,43,58,"PL3"],
            [hoja2,4,4,5,58,"PL_titulos"],
            [hoja3,4,4,12,58,"CF1"],
            [hoja3,16,4,23,58,"CF1"],
            [hoja3,4,4,5,58,"CF_titulos"],
            [hoja4,9,4,50,58,"KPIs1"],
            [hoja4,55,4,77,58,"KPIs2"],
            [hoja4,81,4,86,58,"KPIs3"],
            [hoja4,4,4,5,58,"KPIs_titulos"],
            [hoja1,1,2,11,16,"REPORTING"]
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[6],lista_imagenes2[5])
    juntar_imagenes(lista_imagenes2[10],lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[10],lista_imagenes2[8])
    juntar_imagenes(lista_imagenes2[10],lista_imagenes2[9])

    
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[1]
    plot2=lista_imagenes1[2]

    screen2=lista_imagenes2[0]
    screen3=lista_imagenes2[1]
    screen4=lista_imagenes2[2]
    screen5=lista_imagenes2[4]
    screen6=lista_imagenes2[5]
    screen7=lista_imagenes2[7]
    screen8=lista_imagenes2[8]
    screen9=lista_imagenes2[9]

    #eliminar imágenes auxiliares
    remove(lista_imagenes2[3])
    remove(lista_imagenes2[6])
    remove(lista_imagenes2[10])
    
    lista_slides=[
        [3,(screen1,plot1,plot2)],
        [1,'KPIs'],
        [2,screen7,'KPIs – SAAS Metrics'],
        [2,screen8,'KPIs – Efficiency'],
        [2,screen9,'KPIs – Headcount'],
        [1,"P&L"],
        [2,screen2,'P&L - Revenue and Cost of sales'],
        [2,screen3,'P&L - Fixed costs'],
        [2,screen4,'P&L - Other results'],
        [1,'Cash'],
        [2,screen5,'Operational Cash'],
        [2,screen6,'Financial Cash']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)