
from Functions.funciones import juntar_imagenes, join_image_h, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def ekuore(archivo, guardar_como, template, mes):
    
    # WorkSheets of interest
    hoja1 = "REPORTING->"
    hoja2 = "KPIs"
    hoja3 = "PL"
    hoja4 = "CF"

    lista=[ # Cover
            [hoja1, 5, 2, 40, 16, "Cover"], # 0
            # KPIs
            [hoja2, 6, 4, 39, 58, "KPIs_Human"], # 1
            [hoja2, 40, 4, 65, 58, "KPIs_Vet_1"], # 2
            [hoja2, 66, 4, 86, 58, "KPIs_Vet_2"], # 3
            [hoja2, 4, 4, 5, 58, "KPIs_title"], # 4
            # P&L
            [hoja3, 6, 4, 44, 58, "PL_Revenue"], # 5
            [hoja3, 51, 4, 68, 58, "PL_Costs"], # 6
            [hoja3, 69, 4, 78, 58, "PL_EBITDA"], # 7
            [hoja3, 4, 4, 5, 58, "PL_title"], # 8
            #CFs
            [hoja4, 6, 4, 14, 58, "CF_Operational"], # 9
            [hoja4, 15, 4, 35, 58, "CF_Financial"], # 10
            [hoja4, 4, 4, 5, 58, "CF_title"] # 11
        ]
    
    guardar_en = abspath(join(guardar_como, pardir))
    lista_imagenes1, lista_imagenes2, lista_imagenes_aux = prints_and_plots(archivo, hoja1, lista, guardar_en, resumen_op = False)
    
    # Join images
    ## KPIs
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[3])
    join_image_h(lista_imagenes2[2], lista_imagenes2[3], sep = 0.03)
    ## P&L
    juntar_imagenes(lista_imagenes2[8], lista_imagenes2[5])
    juntar_imagenes(lista_imagenes2[8], lista_imagenes2[6])
    juntar_imagenes(lista_imagenes2[8], lista_imagenes2[7])
    # CF
    juntar_imagenes(lista_imagenes2[11], lista_imagenes2[9])
    juntar_imagenes(lista_imagenes2[11], lista_imagenes2[10])

    #eliminar imágenes auxiliares (headers)
    remove(lista_imagenes2[4])
    remove(lista_imagenes2[8])
    remove(lista_imagenes2[11])
    
    # ppt layout    
    lista_slides=[
        # Cover
        [7, lista_imagenes2[0], 'center', 'Main Picture - Ekuore'],
        # KPIs
        [1,'KPIs'],
        [2, lista_imagenes2[1], 'KPIs – Human Medicine'],
        # [2, lista_imagenes2[2], 'KPIs – Vet Medicine'],
        [2, lista_imagenes2[3], 'KPIs – Vet Medicine'],
        # P&L
        [1,'P&L'],
        [2, lista_imagenes2[5], 'P&L – Revenue'],
        [2, lista_imagenes2[6], 'P&L – Costs'],
        [2, lista_imagenes2[7], 'P&L - EBITDA'],
        # Cash Flow
        [1, 'Cash'],
        [2, lista_imagenes2[9],'Cash - Operational Cash Flow'],
        [2, lista_imagenes2[10],'Cash – Financial Cash Flow']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)