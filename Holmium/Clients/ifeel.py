# -*- coding: utf-8 -*-
"""
Created on Wed Aug 11 15:39:26 2021

@author: romina
"""


from Functions.funciones import juntar_imagenes, prints_and_plots_cell_names,crear_ppt, prints_and_plots
from os.path import join,abspath
from os import pardir, remove

def ifeel(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING->"
    hoja2="KPIs"
    hoja3="PL"
    hoja4="CF"

    lista=[ 
            [hoja1,"Reporting1"],
            [hoja2,"KPIs_titulos"],
            [hoja2,"KPIs_1"],
            [hoja2,"KPIs_2"],
            [hoja2,"KPIs_3"],
            [hoja3,"PL_titulos"],
            [hoja3,"PL_1"],
            [hoja3,"PL_2"],
            [hoja3,"PL_3"],
            [hoja4,"CF_titulos"],
            [hoja4,"CF_1"],
            [hoja4,"CF_2"]
        ]
    
    lista_2 = [[hoja2, 104, 4, 112, 70, "KPIs_4"]]

    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots_cell_names(archivo,hoja1,lista,guardar_en)
    tmp1, lista_imagenes2_KPIs_4, tmp2 = prints_and_plots(archivo, hoja1, lista_2, guardar_en, resumen_op = False)

    juntar_imagenes(lista_imagenes2[1],lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[1],lista_imagenes2[3])
    juntar_imagenes(lista_imagenes2[1],lista_imagenes2[4])
    juntar_imagenes(lista_imagenes2[1],lista_imagenes2_KPIs_4[0])
    juntar_imagenes(lista_imagenes2[5],lista_imagenes2[6])
    juntar_imagenes(lista_imagenes2[5],lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[5],lista_imagenes2[8])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[10])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[11])


    
    screen1=lista_imagenes2[0]
    plot1=lista_imagenes1[2]
    plot2=lista_imagenes1[1]

    screen2=lista_imagenes2[2]
    screen3=lista_imagenes2[3]
    screen4=lista_imagenes2[4]
    screen5=lista_imagenes2_KPIs_4[0]#lista_imagenes2[5]
    screen6=lista_imagenes2[6]
    screen7=lista_imagenes2[7]
    screen8=lista_imagenes2[8]
    screen9=lista_imagenes2[10]
    screen10=lista_imagenes2[11]


    
    
    lista_slides=[
        [3,(screen1,plot1,plot2)],
        [1,'KPIs'],
        [2,screen2,'KPIs – B2B Metrics (MRR)'],
        [2,screen3,'KPIs – B2B Metrics (Accounts)'],
        [2,screen4,'KPIs – B2B Metrics (Efficiency)'],
        [2,screen5,'KPIs – Headcount'],
        [1,"P&L"],
        [2,screen6,'P&L - Revenue and Cost of sales'],
        [2,screen7,'P&L - Fixed costs'],
        [2,screen8,'P&L - Other results'],
        [1,'Cash'],
        [2,screen9,'Operational Cash'],
        [2,screen10,'Financial Cash']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)