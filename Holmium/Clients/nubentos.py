
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 10:04:17 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt, join_image_h
from os.path import join,abspath
from os import pardir, remove

def nubentos(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1 = "REPORTING->"
    hoja2 = "PL"
    hoja3 = "CF"

    lista=[
            [hoja2, 4, 4, 20, 58, "PL1"], # 0
            [hoja2, 21, 4, 41, 58, "PL2"], # 1
            [hoja2, 42, 4, 50, 58, "PL3"], # 2
            [hoja2, 4, 4, 5, 58, "PL_titulos"], # 3
            [hoja3, 4, 4, 33, 58, "CF1"], # 4
            [hoja1, 5, 2, 11, 4, "Cover_ARR"], # 5
            [hoja1, 5, 13, 11, 16, "Cover_Adj"], # 6
            [hoja1, 13, 1, 19, 8, "Cover_Cash"], # 7
            [hoja1, 1, 2, 4, 16, "Cover_brand"] # 8
        ]
    
    guardar_en = abspath(join(guardar_como, pardir))
    lista_imagenes1, lista_imagenes2, x = prints_and_plots(archivo, hoja1, lista, guardar_en)
    juntar_imagenes(lista_imagenes2[3], lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3], lista_imagenes2[2])
    join_image_h(lista_imagenes2[5], lista_imagenes2[6])
    join_image_h(lista_imagenes2[6], lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[8], lista_imagenes2[7])

    #eliminar imágenes auxiliares
    remove(lista_imagenes2[3])
    remove(lista_imagenes2[5])
    remove(lista_imagenes2[6])
    remove(lista_imagenes2[8])


    lista_slides=[
        [4, lista_imagenes2[7]],
        [1, "P&L"],
        [2, lista_imagenes2[0], 'P&L - Revenue and Cost of sales'],
        [2, lista_imagenes2[1], 'P&L - Fixed costs'],
        [2, lista_imagenes2[2], 'P&L - Other results'],
        [1, 'Cash'],
        [2, lista_imagenes2[4], 'Cash']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)