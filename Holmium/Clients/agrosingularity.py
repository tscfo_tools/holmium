# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 18:38:57 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def agrosingularity(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING->"
    hoja2="KPIs"
    hoja3="PL"
    hoja4="CF"

    lista=[
            # KPIs
            [hoja2, 46, 3, 83, 19, "KPI_1"], # 0
            [hoja2, 14, 3, 42, 19, "KPI_2"], # 1
            [hoja2, 4, 3, 5, 19, "KPI_title"], # 2
            # PL
            [hoja3, 45, 4, 51, 60, "PL"],# 3
            [hoja3, 4, 4, 5, 60, "PL_title"], # 4
            # CF
            [hoja4, 6, 4, 28, 58, "CF"], # 5
            [hoja4, 4, 4, 5, 58, "CF_title"], # 6
            # Cover
            [hoja1, 1, 2, 34, 16, "Cover"] # 7
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)
    # KPIs
    juntar_imagenes(lista_imagenes2[2],lista_imagenes2[0])
    juntar_imagenes(lista_imagenes2[2],lista_imagenes2[1])
    # PL
    juntar_imagenes(lista_imagenes2[4],lista_imagenes2[3])
    # CF
    juntar_imagenes(lista_imagenes2[6],lista_imagenes2[5])

    
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[1]
    plot2=lista_imagenes1[2]

    #eliminar imágenes auxiliares
    remove(lista_imagenes2[2])
    remove(lista_imagenes2[4])
    remove(lista_imagenes2[6])
    
    lista_slides=[
        [4, lista_imagenes2[7]],
        [1,'KPIs'],
        [7,lista_imagenes2[0], 'left', 'KPIs'],
        [1,'STOCK'],
        [7,lista_imagenes2[1], 'left', 'KPIs'],
        [1,"P&L"],
        [2,lista_imagenes2[3],'P&L - Other results'],
        [1,'Cash'],
        [2,lista_imagenes2[5],'Cash']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)