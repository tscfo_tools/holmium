from Functions.funciones import juntar_imagenes, join_image_h, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def mediquo(archivo, guardar_como, template, mes):
    
    # WorkSheets of interest
    hoja_1 = "PL"
    hoja_2 = "CF"
    hoja_3 = "KPIs"
    hoja_4 = "KPIs-Telehealth"
    hoja_5 = "KPIs-SaaS"
    hoja_6 = "REPORTING->"

    lista=[ # P&L
            [hoja_1, 6, 4, 30, 70, "PL_Revenue"], # 0
            [hoja_1, 31, 4, 48, 70, "PL_Net_Rev"], # 1
            [hoja_1, 49, 4, 66, 70, "PL_fix"], # 2
            [hoja_1, 67, 4, 76, 70, "PL_EBITDA"], # 3
            [hoja_1, 4, 4, 5, 70, "PL_title"], # 4
            # Cash Flow
            [hoja_2, 6, 4, 27, 71, "CF"], # 5
            [hoja_2, 4, 4, 5, 71, "CF_title"], # 6
            # KPIs
            [hoja_3, 8, 4, 26, 71, "KPI_MRR"], # 7
            [hoja_3, 27, 4, 49, 71, "KPI_Accounts"], # 8
            [hoja_3, 94, 4, 103, 71, "KPI_HC"], # 9
            [hoja_3, 4, 4, 5, 71, "KPI_title"], # 10
            [hoja_3, 6, 4, 7, 71, "KPI_aux"], # 11
            # KPIs - Telehealth
            [hoja_4, 54, 4, 71, 71, "KPI_Telehealth_Eff"], # 12
            [hoja_4, 76, 4, 96, 71, "KPI_Telehealth_Use"], # 13
            [hoja_4, 4, 4, 5, 71, "KPI_Telehealth_title"], # 14
            # KPIs - SaaS
            [hoja_5, 76, 4, 104, 71, "KPI_SaaS"], # 15
            [hoja_5, 4, 4, 5, 71, "KPI_SaaS_title"], # 16
        ]
    
    guardar_en = abspath(join(guardar_como, pardir))
    lista_imagenes1, lista_imagenes2, lista_imagenes_aux = prints_and_plots(archivo, hoja_6, lista, guardar_en, resumen_op = False)
    
    # Join images
    # P&L
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[0])
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[3])
    ## CF
    juntar_imagenes(lista_imagenes2[6], lista_imagenes2[5])
    ## KPIs
    juntar_imagenes(lista_imagenes2[10], lista_imagenes2[11])
    juntar_imagenes(lista_imagenes2[11], lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[11], lista_imagenes2[8])
    juntar_imagenes(lista_imagenes2[10], lista_imagenes2[9])
    # KPIs - Telehealth
    juntar_imagenes(lista_imagenes2[14], lista_imagenes2[12])
    juntar_imagenes(lista_imagenes2[14], lista_imagenes2[13])
    # KPIs - SaaS
    juntar_imagenes(lista_imagenes2[16], lista_imagenes2[15])

    #eliminar imágenes auxiliares (headers)
    remove(lista_imagenes2[4])
    remove(lista_imagenes2[6])
    remove(lista_imagenes2[10])
    remove(lista_imagenes2[11])
    remove(lista_imagenes2[14])
    remove(lista_imagenes2[16])
    
    # ppt layout    
    lista_slides=[
        # P&L
        [1,'P&L'],
        # f(8, slide_id, image_paths, h_ratio, w_ratio, top_space, text)
        [8, 3, [lista_imagenes2[0]], [0.6], [0.9], [0.12], ''],
        [8, 3, [lista_imagenes2[1]], [0.6], [0.8], [0.12], ''],
        [8, 3, [lista_imagenes2[2]], [0.6], [0.9], [0.12], ''],
        [8, 3, [lista_imagenes2[3]], [0.5], [0.9], [0.15], ''],
        # Cash Flow
        [1, 'Cash Flow'],
        [8, 3, [lista_imagenes2[5]], [0.6], [0.9], [0.12], ''],
        # P&L
        [1,'KPIs'],
        [8, 3, [lista_imagenes2[7]], [0.6], [0.9], [0.12], ''],
        [8, 3, [lista_imagenes2[8]], [0.6], [0.9], [0.12], ''],
        [8, 3, [lista_imagenes2[9]], [0.5], [0.8], [0.17], ''],
        [8, 2, [lista_imagenes2[12]], [0.55], [0.9], [0.15], 'Telehealth'],
        [8, 2, [lista_imagenes2[13]], [0.4], [0.9], [0.15], 'Telehealth'],
        [8, 2, [lista_imagenes2[15]], [0.6], [0.9], [0.15], 'SaaS']
    ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)