# -*- coding: utf-8 -*-
"""
Created on Thu Aug 12 09:45:17 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def iomob(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING->"
    hoja2="PL"
    hoja3="CF"
    hoja4="KPIs"

    lista=[
            [hoja2,6,4,17,58,"PL1"],#0
            [hoja2,24,4,41,58,"PL2"],
            [hoja2,42,4,53,58,"PL3"],
            [hoja2,4,4,5,58,"PL_titulos"],
            [hoja3,6,4,14,60,"CF1"],#4
            [hoja3,15,4,28,60,"CF2"],
            [hoja3,4,4,5,60,"CF_titulos"],
            [hoja4,8,4,13,58,"KPIs1_1"], # 7
            [hoja4,42,4,46,58,"KPIs1_2"],
            [hoja4,49,4,72,58,"KPIs2"],
            [hoja4,75,4,83,58,"KPIs3"],
            [hoja4,4,4,5,58,"KPIs_titulos"], # 11
            [hoja1,1,2,17,16,"REPORTING"]
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[0])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[6],lista_imagenes2[4])
    juntar_imagenes(lista_imagenes2[6],lista_imagenes2[5])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[7],lista_imagenes2[8])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[9])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[10])



    
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[1]
    plot2=lista_imagenes1[2]

    screen2=lista_imagenes2[0]
    screen3=lista_imagenes2[1]
    screen4=lista_imagenes2[2]
    screen5=lista_imagenes2[4]
    screen6=lista_imagenes2[5]
    screen7=lista_imagenes2[8]
    screen8=lista_imagenes2[9]
    screen9=lista_imagenes2[10]


    #eliminar imágenes auxiliares
    remove(lista_imagenes2[3])
    remove(lista_imagenes2[6])
    remove(lista_imagenes2[11])
    
    
    lista_slides=[
        [3,(screen1,plot1,plot2)],
        [1,'KPIs'],
        [2,screen7,'KPIs – SAAS Metrics', 24],
        [2,screen8,'KPIs - Efficiency', 14],
        [2,screen9,'KPIs - Headcount', 24],
        [1,"P&L"],
        [2,screen2,'P&L - Revenue and Cost of sales', 24],
        [2,screen3,'P&L - Fixed costs', 20],
        [2,screen4,'P&L - Other results', 26],
        [1,'Cash'],
        [2,screen5,'Operational Cash', 28],
        [2,screen6,'Financial Cash', 22]
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)