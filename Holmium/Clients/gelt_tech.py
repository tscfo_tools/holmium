# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 17:19:19 2021

@author: romin
"""


from os.path import join,abspath
from os import pardir

from Functions.funciones import juntar_imagenes, prints_and_plots_cell_names,crear_ppt


def gelt_tech(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    hoja1="REPORTING->"
    hoja3="PL"
    hoja4="CF"
    
    lista=[
            
            [hoja3,"PL_Titulos"],
            [hoja3,"PL_1"],
            [hoja3,"PL_2"],
            [hoja3,"PL_3"],
            [hoja4,"CF_Titulos"],
            [hoja4,"CF_1"],
            [hoja4,"CF_2"]
        ]
    
    
    guardar_en=abspath(join(guardar_como, pardir))
    imagenes1,imagenes2,lista_imagenes3=prints_and_plots_cell_names(archivo,hoja1,lista,guardar_en)
    #juntar imagenes con sus títulos correspondientes
    
    juntar_imagenes(imagenes2[0],imagenes2[1])
    juntar_imagenes(imagenes2[0],imagenes2[2])
    juntar_imagenes(imagenes2[0],imagenes2[3])
    juntar_imagenes(imagenes2[4],imagenes2[5])
    juntar_imagenes(imagenes2[4],imagenes2[6])
    

    screen1=imagenes2[1]
    screen2=imagenes2[2]
    screen3=imagenes2[3]
    screen4=imagenes2[5]
    screen5=imagenes2[6]
    
    
    lista_slides=[

        [1,'P&L'],
        [2,screen1,'P&L - Revenue and Cost of sales',28.95],
        [2,screen2,'P&L - Fixed costs',26.7],
        [2,screen3,'P&L - Other results',26.25],
        [1,'Cash'],
        [2,screen4,'Cash',24.47],   
        [2,screen5,"Cash",27.1]
        ]
    
    
    crear_ppt(template,lista_slides,guardar_como,mes)