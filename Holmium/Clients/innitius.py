
"""
Created on Wed Aug 11 17:20:25 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots_cell_names,crear_ppt
from os.path import join,abspath
from os import pardir

def innitius(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING-> "
    hoja2="KPIs"
    hoja3="PL"
    hoja4="CF"
    
    lista=[
            [hoja1,"titulo"],
            [hoja1,"resumen"],
            [hoja2,"KPIs_titulos"],
            [hoja2, "Headcount"],
            [hoja3,"PL_1"],
            [hoja3,"PL_titulos"],
            [hoja3,"PL_2"],
            [hoja3,"PL_3"],
            [hoja3,"PL_4"],
            [hoja4,"CF_1"],
            [hoja4,"CF_2"],
            [hoja4,"CF_titulos"]
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,lista_imagenes3=prints_and_plots_cell_names(archivo,hoja1,lista,guardar_en)
    juntar_imagenes(lista_imagenes2[2],lista_imagenes2[3])
    juntar_imagenes(lista_imagenes2[5],lista_imagenes2[6])
    juntar_imagenes(lista_imagenes2[5],lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[5],lista_imagenes2[8])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[10])
    


    titulo=lista_imagenes2[0]
    resumen=lista_imagenes2[1]    

    plot1=lista_imagenes1[3]
    plot2=lista_imagenes1[4]
    
    kpis=lista_imagenes2[3]    
    
    pl1=lista_imagenes2[4]
    pl2=lista_imagenes2[6]
    pl3=lista_imagenes2[7]
    pl4=lista_imagenes2[8]

    cf1=lista_imagenes2[9]
    cf2=lista_imagenes2[10]
    
    lista_slides=[
        [5,(titulo,resumen),(0.43,0.43),(1.62,5.52),(32.88,32.88)],
        [5,(titulo,plot1,plot2),(0.43,0.83,16.93),(1.62,5.02,5.02),(32.88,16.05,16.35)],
        [1,'KPIs'],
        [2,kpis,'KPIs – Headcount',26.03],
        [1,"P&L"],
        [2,pl1,'P&L - Revenue and Cost of sales',23.4],
        [2,pl2,'P&L - Personnel costs',29.98],
        [2,pl3,'P&L - Operating costs',29.95],
        [2,pl4,'P&L - Other results',30.22],
        [1,'Cash'],
        [2,cf1,'Operational Cash',32.6],
        [2,cf2,'Financial Cash',32.6]
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)