# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 16:34:41 2021

@author: romina
"""
from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def plantarse(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING->"
    hoja2="PL"
    hoja3="CF"


    lista=[
            [hoja2,8,3,28,58,"PL1"],
            [hoja2,32,3,47,58,"PL2"],
            [hoja2,53,3,68,58,"PL3"],
            [hoja2,70,3,78,58,"PL4"],
            [hoja2,1,3,5,58,"PL_titulos"],
            [hoja3,1,3,25,58,"CF1"],
           
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)
    
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[0])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[2])
    
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[1]
    plot2=lista_imagenes1[2]

    screen2=lista_imagenes2[0]
    screen3=lista_imagenes2[1]
    screen4=lista_imagenes2[2]
    screen5=lista_imagenes2[3]
    screen6=lista_imagenes2[5]   

    #eliminar imágenes auxiliares
    remove(lista_imagenes2[4])
    
    lista_slides=[
        [3,(screen1,plot1,plot2)],
        [1,"P&L"],
        [2,screen2,'P&L - Revenue and Cost of sales - PLANTARSE'],
        [2,screen3,'P&L - Revenue and Cost of sales - PLANTARSE-GONG'],
        [2,screen4,'P&L - Fixed costs'],
        [2,screen5,'P&L - Other results'],
        [1,'Cash'],
        [2,screen6,'Cash']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)