# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 12:26:45 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def validated(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING->"
    hoja2="PL"
    hoja3="CF"
    hoja4="KPIs"

    lista=[
            [hoja2,1,3,23,70,"PL1"],
            [hoja2,30,3,61,70,"PL2"],
            [hoja2,62,3,71,70,"PL3"],
            [hoja2,1,3,5,70,"PL_titulos"],
            [hoja3,1,3,29,70,"CF1"],
            [hoja4,8,3,13,70,"KPIs1.1"],
            [hoja4,27,3,28,70,"KPIs1.2"],
            [hoja4,39,3,53,70,"KPIs1"],
            [hoja4,56,3,61,70,"KPIs2.1"],
            [hoja4,67,3,68,70,"KPIs2.2"],
            [hoja4,76,3,77,70,"KPIs2.3"],
            [hoja4,89,3,90,70,"KPIs2.4"],
            [hoja4,98,3,101,70,"KPIs2"],
            [hoja4,104,3,119,70,"KPIs3"],
            [hoja4,1,3,5,70,"KPIs_titulos"]
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[5],lista_imagenes2[6])
    juntar_imagenes(lista_imagenes2[6],lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[8],lista_imagenes2[9])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[10])
    juntar_imagenes(lista_imagenes2[10],lista_imagenes2[11])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[12])
    juntar_imagenes(lista_imagenes2[14],lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[14],lista_imagenes2[12])
    juntar_imagenes(lista_imagenes2[14],lista_imagenes2[13])
    

    
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[1]
    plot2=lista_imagenes1[2]

    screen2=lista_imagenes2[0]
    screen3=lista_imagenes2[1]
    screen4=lista_imagenes2[2]
    screen5=lista_imagenes2[4]
    screen6=lista_imagenes2[7]
    screen7=lista_imagenes2[12]
    screen8=lista_imagenes2[13]
   

    #eliminar imágenes auxiliares
    remove(lista_imagenes2[3])
    remove(lista_imagenes2[5])
    remove(lista_imagenes2[6])
    remove(lista_imagenes2[8])
    remove(lista_imagenes2[9])
    remove(lista_imagenes2[10])
    remove(lista_imagenes2[11])
    remove(lista_imagenes2[14])
    
    lista_slides=[
        [3,(screen1,plot1,plot2)],
        [1,'KPIs'],
        [2,screen6,'KPIs – SAAS Metrics'],
        [2,screen7,'KPIs – Efficiency'],
        [2,screen8,'KPIs – Headcount'],
        [1,"P&L"],
        [2,screen2,'P&L - Revenue and Cost of sales'],
        [2,screen3,'P&L - Fixed costs'],
        [2,screen4,'P&L - Other results'],
        [1,'Cash'],
        [2,screen5,'Cash']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)