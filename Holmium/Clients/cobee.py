# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 18:45:42 2021

@author: romina

Crear ppt para Cobee
"""


from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir


#PROBAR
#prints_and_plots(archivo,hoja_resumen,lista_part_prints,carpeta_cliente)
def cobee(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING"
    hoja2="PL-GLOBAL"
    hoja3="CF"
    hoja4="KPIs"

    lista=[
            [hoja2,1,1,42,70,"PL1"],
            [hoja2,43,1,58,70,"PL2"],
            [hoja2,59,1,69,70,"PL3"],
            [hoja2,1,1,5,70,"PL_titulos"],
            [hoja3,1,1,36,70,"CF1"],
            [hoja4,1,1,31,70,"KPIs1"],
            [hoja4,32,1,52,70,"KPIs2"],
            [hoja4,54,1,73,70,"KPIs3"],
            [hoja4,74,1,83,70,"KPIs4"],
            [hoja4,1,1,5,70,"KPIs_titulos"],
            [hoja1,1,1,12,24,"REPORTING"]
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[6])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[8])
    
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[7]
    plot2=lista_imagenes1[6]
    screen2=lista_imagenes2[0]
    screen3=lista_imagenes2[1]
    screen4=lista_imagenes2[2]
    screen5=lista_imagenes2[4]
    screen6=lista_imagenes2[5]
    screen7=lista_imagenes2[6]
    screen8=lista_imagenes2[7]
    screen9=lista_imagenes2[8]
    
    lista_slides=[
        [3,(screen1,plot1,plot2)],
        [1,'KPIs'],
        [2,screen6,'KPIs – Companies Metrics'],
        [2,screen7,'KPIs – Activity Metrics'],
        [2,screen8,'KPIs – Efficiency'],
        [2,screen9,'KPIs – Headcount'],
        [1,"P&L"],
        [2,screen2,'P&L - Revenue and Cost of sales'],
        [2,screen3,'P&L - Fixed costs'],
        [2,screen4,'P&L - Revenue and Cost of sales'],
        [1,'Cash'],
        [2,screen5,'Cash']
        
        ]
    
    
    crear_ppt(template,lista_slides,guardar_como,mes)
    
# template=r"G:\My Drive\Python\Presentaciones\clients\Cobee\template\cobee.pptx"
# archivo=r"G:\.shortcut-targets-by-id\10myIjzDOsGUcukpuwOBx50bAEyy-EViv\3.Clients\R3.Cobee\21\2105\2105 Cobee_Reporting_v4.xlsx"
# guardar_como=r"G:\My Drive\Python\Presentaciones\clients\Cobee\prueba.pptx"

# cobee(archivo,guardar_como,template)