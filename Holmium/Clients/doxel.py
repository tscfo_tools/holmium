from Functions.funciones import juntar_imagenes, join_image_h, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def doxel(archivo, guardar_como, template, mes):
    
    # WorkSheets of interest
    hoja1 = "REPORTING->"
    hoja2 = "PL"
    hoja3 = "CF"
    hoja4 = "Cash"

    lista=[ # P&L
            [hoja2, 6, 6, 37, 58, "PL_rev"], # 0
            [hoja2, 43, 6, 66, 58, "PL_costs"], # 1
            [hoja2, 67, 6, 78, 58, "PL_ebitda"], # 2
            [hoja2, 2, 6, 5, 58, "PL_title"], # 3
            # CF
            [hoja3, 6, 6, 18, 58, "CF_op"], # 4
            [hoja3, 19, 6, 33, 58, "CF_fin"], # 5
            [hoja3, 2, 6, 5, 58, "CF_title"], # 6
            # CF forecast
            [hoja4, 6, 6, 18, 75, "FC_op"], # 7
            [hoja4, 19, 6, 33, 75, "FC_fin"], # 8
            [hoja4, 2, 6, 5, 75, "FC_title"] # 9
        ]
    
    guardar_en = abspath(join(guardar_como, pardir))
    lista_imagenes1, lista_imagenes2, lista_imagenes_aux = prints_and_plots(archivo, hoja1, lista, guardar_en)
    
    # Join images
    ## P&L
    juntar_imagenes(lista_imagenes2[3], lista_imagenes2[0])
    juntar_imagenes(lista_imagenes2[3], lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3], lista_imagenes2[2])
    # CF
    juntar_imagenes(lista_imagenes2[6], lista_imagenes2[4])
    juntar_imagenes(lista_imagenes2[6], lista_imagenes2[5])
    # CF forecast
    juntar_imagenes(lista_imagenes2[9], lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[9], lista_imagenes2[8])

    #eliminar imágenes auxiliares (headers)
    remove(lista_imagenes2[3])
    remove(lista_imagenes2[6])
    remove(lista_imagenes2[9])
    
    # ppt layout    
    lista_slides=[
        # P&L
        [1,'P&L'],
        [2, lista_imagenes2[0], 'P&L - Revenue'],
        [2, lista_imagenes2[1], 'P&L - Costs'],
        [2, lista_imagenes2[2], 'P&L - EBITDA'],
        # CF
        [1, 'Cash'],
        [2, lista_imagenes2[4],'Cash - Operational Cash Flow'],
        [2, lista_imagenes2[5],'Cash – Financial Cash Flow'],
        # CF forecast
        [1, 'Cash Forecast'],
        [2, lista_imagenes2[7],'Cash - Operational CF Forecast'],
        [2, lista_imagenes2[8],'Cash – Financial CF Forecast']
    ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)