
from Functions.funciones import juntar_imagenes, join_image_h, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def baia(archivo,guardar_como,template,mes):
    
    # WorkSheets of interest
    hoja1= "Cover Reporting->"
    hoja2= "KPIs"
    hoja3= "PL"
    hoja4= "CF"

    lista=[ # KPIs
            [hoja2, 6, 3, 47, 16, "KPI_metrics"], # 0
            [hoja2, 48, 3, 110, 16, "KPI_HC"], # 1
            [hoja2, 4, 3, 5, 16, "KPI_title"], # 2
            # P&L
            [hoja3, 6, 4, 27, 60, "PL_GrossMargin"], # 3
            [hoja3, 28, 4, 45, 60, "PL_Fixed"], # 4
            [hoja3, 50, 4, 55, 60, "PL_Other"], # 5
            [hoja3, 59, 4, 93, 60, "PL_Miraculin"], # 6
            [hoja3, 97, 4, 114, 60, "PL_Baia1"], # 7
            [hoja3, 116, 4, 135, 60, "PL_Baia2"], # 8
            [hoja3, 4, 4, 5, 60, "PL_title"], # 9
            #CFs
            [hoja4, 6, 4, 21, 60, "Cash1"], # 10
            [hoja4, 22, 4, 35, 60, "Cash2"], # 11
            [hoja4, 4, 4, 5, 60, "CF_title"] # 12
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1, lista_imagenes2, lista_imagenes_aux = prints_and_plots(archivo,hoja1,lista,guardar_en)
    
    # Join images
    ## KPIs
    juntar_imagenes(lista_imagenes2[2], lista_imagenes2[0])
    juntar_imagenes(lista_imagenes2[2], lista_imagenes2[1])
    # P&L
    juntar_imagenes(lista_imagenes2[9], lista_imagenes2[3])
    juntar_imagenes(lista_imagenes2[9], lista_imagenes2[4])
    juntar_imagenes(lista_imagenes2[9], lista_imagenes2[5])
    juntar_imagenes(lista_imagenes2[9], lista_imagenes2[6])
    juntar_imagenes(lista_imagenes2[9], lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[9], lista_imagenes2[8])
    ## CF
    juntar_imagenes(lista_imagenes2[12], lista_imagenes2[10])
    juntar_imagenes(lista_imagenes2[12], lista_imagenes2[11])

    #eliminar imágenes auxiliares (headers)
    remove(lista_imagenes2[2])
    remove(lista_imagenes2[9])
    remove(lista_imagenes2[12])
    
    # ppt layout    
    lista_slides=[
        # KPIs
        [6, 'KPIs', lista_imagenes2[0], 2.5, 4, 12], # [6, text, (screen1,screen2,screen3),(pos_x1,pos_x2,pos_x3),(y1,y2,y3),(largo1,largo2,largo3)]
        [2, lista_imagenes2[1], 'KPIs'],
        # P&L
        [2, lista_imagenes2[3], 'P&L – Gross Margin'],
        [2, lista_imagenes2[4], 'P&L – Fixed Costs'],
        [2, lista_imagenes2[5], 'P&L – Other Results'],
        [6, 'P&L MIRACULIN', lista_imagenes2[6], 2, 4.5, 19],
        [2, lista_imagenes2[7], 'P&L BAIA'],
        [2, lista_imagenes2[8], 'P&L BAIA'],
        # Cash
        [2,lista_imagenes2[10],'Cash'],
        [2,lista_imagenes2[11],'Cash']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)