# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 11:46:13 2021

@author: romina
"""
from Functions.funciones import juntar_imagenes, join_image_h, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def gasmobi(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1= "REPORTING->"
    hoja2= "KPIs"
    hoja3= "PL-GLOBAL"
    hoja4= "CF"

    lista=[ # KPIs
            [hoja2, 12, 4, 35, 21, "KPI_ad"], # 0
            [hoja2, 53, 4, 76, 21, "KPI_pub"], # 1
            [hoja2, 90, 4, 101, 21, "KPI_eff"], # 2
            [hoja2, 104, 4, 112, 21, "KPI_HC"], # 3
            [hoja2, 4, 4, 5, 21, "KPI_title"], # 4
            # PLs
            [hoja3, 6, 4, 32, 58, "PL_Sales"], # 5
            [hoja3, 33, 4, 50, 58, "PL_Fixed"], # 6
            [hoja3, 51, 4, 71, 58, "PL_Other"], # 7
            [hoja3, 4, 4, 5, 58, "PL_title"], # 8
            #CFs
            [hoja4, 4, 4, 31, 58,"CF"], # 9
            [hoja1, 1, 2, 36, 20,"Cover"] # 10
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1, lista_imagenes2, lista_imagenes_aux = prints_and_plots(archivo,hoja1,lista,guardar_en)
    
    # Join images
    ## KPIs
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[0])
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[1])
    join_image_h(lista_imagenes2[0], lista_imagenes2[1], sep = 0.1)
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[3])

    # PLs
    juntar_imagenes(lista_imagenes2[8], lista_imagenes2[5])
    juntar_imagenes(lista_imagenes2[8], lista_imagenes2[6])
    juntar_imagenes(lista_imagenes2[8], lista_imagenes2[7])

    #eliminar imágenes auxiliares
    remove(lista_imagenes2[4])
    remove(lista_imagenes2[8])
    
    # Cover
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[1]
    plot2=lista_imagenes1[2]

    # Ppt layout    
    lista_slides=[
        [4, lista_imagenes2[10]],
        [1,'KPIs'],
        [2,lista_imagenes2[1],'KPIs'],
        [2,lista_imagenes2[2],'KPIs – Efficiency'],
        [2,lista_imagenes2[3],'KPIs – Headcount'],
        [1,"P&L"],
        [2,lista_imagenes2[5],'P&L - Revenue and Cost of sales'],
        [2,lista_imagenes2[6],'P&L - Fixed costs'],
        [2,lista_imagenes2[7],'P&L - Other results'],
        [1,'Cash'],
        [2,lista_imagenes2[9],'Cash']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)