from Functions.funciones import juntar_imagenes, join_image_h, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def liux(archivo, guardar_como, template, mes):
    
    # WorkSheets of interest
    hoja1= "REPORTING->"
    hoja2= "PL"
    hoja3= "CF"

    lista=[ # P&L
            [hoja2, 34, 4, 53, 58, "PL_fix"], # 0
            [hoja2, 54, 4, 65, 58, "PL_ebitda"], # 1
            [hoja2, 4, 4, 5, 58, "PL_title"], # 2
            #CFs
            [hoja3, 6, 4, 18, 58, "CF_op"], # 3
            [hoja3, 19, 4, 33, 58, "CF_fin"], # 4
            [hoja3, 4, 4, 5, 58, "CF_title"] # 5
        ]
    
    guardar_en = abspath(join(guardar_como, pardir))
    lista_imagenes1, lista_imagenes2, lista_imagenes_aux = prints_and_plots(archivo, hoja1, lista, guardar_en)
    
    # Join images
    ## P&L
    juntar_imagenes(lista_imagenes2[2], lista_imagenes2[0])
    juntar_imagenes(lista_imagenes2[2], lista_imagenes2[1])
    # CF
    juntar_imagenes(lista_imagenes2[5], lista_imagenes2[3])
    juntar_imagenes(lista_imagenes2[5], lista_imagenes2[4])

    #eliminar imágenes auxiliares (headers)
    remove(lista_imagenes2[2])
    remove(lista_imagenes2[5])
    
    # ppt layout    
    lista_slides=[
        # P&L
        [1,'P&L'],
        [2, lista_imagenes2[0], 'P&L - Fixed Cost'],
        [2, lista_imagenes2[1], 'P&L - EBITDA'],
        # Cash Flow
        [1, 'Cash'],
        [2, lista_imagenes2[3],'Cash - Operational Cash Flow'],
        [2, lista_imagenes2[4],'Cash – Financial Cash Flow']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)