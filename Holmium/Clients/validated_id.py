
from Functions.funciones import juntar_imagenes, join_image_h, prints_and_plots, crear_ppt
from os.path import join,abspath
from os import pardir, remove

def validated_id(archivo, guardar_como, template, mes):
    
    # WorkSheets of interest
    hoja1 = "REPORTING->"
    hoja2 = "KPIs"
    hoja3 = "PL"
    hoja4 = "CF"

    lista=[ # Cover
            [hoja1, 2, 2, 36, 24, "Cover"], # 0
            # P&L
            [hoja3, 6, 4, 29, 70, "PL_Revenue"], # 1
            [hoja3, 30, 4, 61, 70, "PL_Costs"], # 2
            [hoja3, 62, 4, 71, 70, "PL_EBITDA"], # 3
            [hoja3, 4, 4, 5, 70, "PL_title"], # 4
            #CFs
            [hoja4, 6, 4, 15, 70, "CF_Operational"], # 5
            [hoja4, 15, 4, 31, 70, "CF_Financial"], # 6
            [hoja4, 4, 4, 5, 70, "CF_title"], # 7
            # KPIs
            [hoja2, 6, 4, 26, 72, "KPIs_MRR"], # 8
            [hoja2, 27, 4, 53, 72, "KPIs_Accounts"], # 9
            [hoja2, 56, 4, 75, 72, "KPIs_Eff"], # 10
            [hoja2, 102, 4, 109, 72, "KPIs_Eff_2"], # 11
            [hoja2, 4, 4, 5, 72, "KPIs_title"], # 12
            # KPIs - Cohorts
            ["Cohorts", 38, 2, 65, 15, "KPIs_Cohorts_MRR"], # 13
            ["Cohorts-ES", 38, 2, 65, 15, "KPIs_Cohorts_MRR_ESP"], # 14
            ["Cohorts", 8, 32, 35, 45, "KPIs_Cohorts_ARPA"], # 15
            ["Cohorts-ES", 8, 32, 35, 45, "KPIs_Cohorts_ARPA_ESP"] # 16
        ]
    
    guardar_en = abspath(join(guardar_como, pardir))
    lista_imagenes1, lista_imagenes2, lista_imagenes_aux = prints_and_plots(archivo, hoja1, lista, guardar_en, resumen_op = False)
    
    # Join images
    ## P&L
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[4], lista_imagenes2[3])
    ## P&L
    juntar_imagenes(lista_imagenes2[7], lista_imagenes2[5])
    juntar_imagenes(lista_imagenes2[7], lista_imagenes2[6])
    # KPIs
    juntar_imagenes(lista_imagenes2[12], lista_imagenes2[8])
    juntar_imagenes(lista_imagenes2[12], lista_imagenes2[9])
    juntar_imagenes(lista_imagenes2[12], lista_imagenes2[10])
    juntar_imagenes(lista_imagenes2[12], lista_imagenes2[11])
    # KPIs - Cohorts
    join_image_h(lista_imagenes2[13], lista_imagenes2[14], sep = 0.05)
    join_image_h(lista_imagenes2[15], lista_imagenes2[16], sep = 0.05)

    #eliminar imágenes auxiliares (headers)
    remove(lista_imagenes2[4])
    remove(lista_imagenes2[7])
    remove(lista_imagenes2[12])
    remove(lista_imagenes2[13])
    remove(lista_imagenes2[15])
    
    # ppt layout    
    lista_slides=[
        # Cover
        [7, lista_imagenes2[0], 'center', 'Main Picture - VID'],
        # P&L
        [1,'P&L'],
        [2, lista_imagenes2[1], 'P&L – Revenue'],
        [2, lista_imagenes2[2], 'P&L – Fixed Costs'],
        [2, lista_imagenes2[3], 'P&L - EBITDA'],
        # Cash Flow
        [1, 'Cash'],
        [2, lista_imagenes2[5],'Cash - Operational Cash Flow'],
        [2, lista_imagenes2[6],'Cash – Financial Cash Flow'],
        # KPIs
        [1,'KPIs'],
        [2, lista_imagenes2[8], 'KPIs – MRR'],
        [2, lista_imagenes2[9], 'KPIs – Accounts'],
        [2, lista_imagenes2[10], 'KPIs – Efficiency'],
        [2, lista_imagenes2[11], 'KPIs – Efficiency (II)'],
        [2, lista_imagenes2[14], 'KPIs – Cohorts'],
        [2, lista_imagenes2[16], 'KPIs – Cohorts (II)']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)