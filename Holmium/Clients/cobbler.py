# -*- coding: utf-8 -*-
"""
Created on Wed Aug 11 16:14:06 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def cobbler(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING->"
    hoja2="PL"
    hoja3="CF"
    hoja4="KPIs"

    lista=[
            [hoja2,4,4,18,58,"PL1"],
            [hoja2,21,4,35,58,"PL2"],
            [hoja2,37,4,44,58,"PL3"],
            [hoja2,4,4,5,58,"PL_titulos"],
            [hoja3,4,4,14,58,"CF1"],
            [hoja3,16,4,26,58,"CF2"],
            [hoja3,4,4,5,58,"CF_titulos"],
            [hoja4,10,4,21,19,"KPIs1"],
            [hoja4,25,4,41,19,"KPIs2"],
            [hoja4,45,4,48,19,"KPIs3"],
            [hoja4,60,4,65,19,"KPIs4"],
            [hoja4,4,4,5,19,"KPIs_titulos"],
            [hoja1,1,2,11,20,"REPORTING"]
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[6],lista_imagenes2[5])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[8])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[9])
    juntar_imagenes(lista_imagenes2[11],lista_imagenes2[10])

    
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[1]
    plot2=lista_imagenes1[2]

    screen2=lista_imagenes2[0]
    screen3=lista_imagenes2[1]
    screen4=lista_imagenes2[2]
    screen5=lista_imagenes2[4]
    screen6=lista_imagenes2[5]
    screen7=lista_imagenes2[7]
    screen8=lista_imagenes2[8]
    screen9=lista_imagenes2[9]
    screen10=lista_imagenes2[10]

    #eliminar imágenes auxiliares
    remove(lista_imagenes2[3])
    remove(lista_imagenes2[6])
    remove(lista_imagenes2[11])
    
    
    lista_slides=[
        [3,(screen1,plot1,plot2)],
        [1,'KPIs'],
        [2,screen7,'KPIs – Key Ratios & Drivers'],
        [2,screen8,'KPIs – Business Data'],
        [2,screen9,'KPIs – Efficiency'],
        [2,screen10,'KPIs – Headcount'],
        [1,"P&L"],
        [2,screen2,'P&L - Revenue and Cost of sales'],
        [2,screen3,'P&L - Fixed costs'],
        [2,screen4,'P&L - Other results'],
        [1,'Cash'],
        [2,screen5,'Operational Cash'],
        [2,screen6,'Financial Cash']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)