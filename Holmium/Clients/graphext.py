# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 16:09:01 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def graphext(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING->"
    hoja2="PL"
    hoja3="CF"
    hoja4="KPIs"

    lista=[
            [hoja2,4,3,16,58,"PL1"],
            [hoja2,17,3,33,58,"PL2"],
            [hoja2,34,3,43,58,"PL3"],
            [hoja2,4,3,5,58,"PL_titulos"],
            [hoja3,4,3,14,58,"CF1"],
            [hoja4,8,3,26,58,"KPIs1"],
            [hoja4,27,3,38,58,"KPIs2"],
            [hoja4,52,3,75,58,"KPIs3"],
            [hoja4,78,3,85,58,"KPIs4"],
            [hoja4,4,3,5,58,"KPIs_titulos"],
            [hoja1,1,2,13,20,"REPORTING"],
            [hoja3,15,3,25,58,"CF2"],
            [hoja3,4,3,5,58,"CF_titulos"]
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[5])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[6])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[8])
    juntar_imagenes(lista_imagenes2[12],lista_imagenes2[11])

    
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[1]
    plot2=lista_imagenes1[2]

    screen2=lista_imagenes2[0]
    screen3=lista_imagenes2[1]
    screen4=lista_imagenes2[2]
    screen5=lista_imagenes2[4]
    screen6=lista_imagenes2[5]
    screen7=lista_imagenes2[6]
    screen8=lista_imagenes2[7]
    screen9=lista_imagenes2[8]
    screen10=lista_imagenes2[11]

    #eliminar imágenes auxiliares
    remove(lista_imagenes2[3])
    remove(lista_imagenes2[9])
    
    lista_slides=[
        [3,(screen1,plot1,plot2),(0.53,1.33,16.93),(0,9.52,9.52),(32.81,15.6,15.6)],
        [1,'KPIs'],
        [2,screen6,'KPIs – SAAS Metrics (MRR)',22.59],
        [2,screen7,'KPIs – SAAS Metrics (Accounts)',27.28],
        [2,screen8,'KPIs – Efficiency',20.55],
        [2,screen9,'KPIs – Headcount',28.03],
        [1,"P&L"],
        [2,screen2,'P&L - Revenue and Cost of sales',27.25],
        [2,screen3,'P&L - Fixed costs',26.02],
        [2,screen4,'P&L - Other results & EBITDA',28.86],
        [1,'Cash'],
        [2,screen5,'Cash – Operational cash flow',27.47],
        [2,screen10,'Cash – Total cash Flow and position EoP',27.47]
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)