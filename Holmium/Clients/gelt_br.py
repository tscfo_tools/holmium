# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 16:08:58 2021

@author: romin
"""

from os.path import join,abspath
from os import pardir

from Functions.funciones import juntar_imagenes, prints_and_plots_cell_names,crear_ppt


def gelt_br(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    hoja1="REPORTING->"
    hoja2="KPIs"
    hoja3="PL"
    hoja4="CF"
    
    lista=[
            [hoja1,"Print_Area_cover"],
            [hoja2,"KPIs_Titulos"],
            [hoja2, "KPIs_1"],
            [hoja2, "KPIs_2"],
            [hoja2, "KPIs_3"],
            [hoja2, "KPIs_4"],
            [hoja2, "KPIs_5"],
            [hoja3,"PL_Titulos"],
            [hoja3,"PL_1"],
            [hoja3,"PL_2"],
            [hoja3,"PL_3"],
            [hoja4,"CF_Titulos"],
            [hoja4,"CF_1"],
            [hoja4,"CF_2"]
        ]
    
    
    guardar_en=abspath(join(guardar_como, pardir))
    imagenes1,imagenes2,lista_imagenes3=prints_and_plots_cell_names(archivo,hoja1,lista,guardar_en)
    #juntar imagenes con sus títulos correspondientes
    
    juntar_imagenes(imagenes2[1],imagenes2[2])
    juntar_imagenes(imagenes2[1],imagenes2[3])
    juntar_imagenes(imagenes2[1],imagenes2[4])
    juntar_imagenes(imagenes2[1],imagenes2[5])
    juntar_imagenes(imagenes2[1],imagenes2[6])
    juntar_imagenes(imagenes2[7],imagenes2[8])
    juntar_imagenes(imagenes2[7],imagenes2[9])
    juntar_imagenes(imagenes2[7],imagenes2[10])
    juntar_imagenes(imagenes2[11],imagenes2[12])
    juntar_imagenes(imagenes2[11],imagenes2[13])
    
    screen1=imagenes2[0]
    plot1=imagenes1[2]
    plot2=imagenes1[3]
    screen2=imagenes2[2]
    screen3=imagenes2[3]
    screen4=imagenes2[4]
    screen5=imagenes2[5]
    screen6=imagenes2[6]
    screen7=imagenes2[8]
    screen8=imagenes2[9]
    screen9=imagenes2[10]
    screen10=imagenes2[12]
    screen11=imagenes2[13]
    
    lista_slides=[
        [3,(screen1,plot1,plot2)],
        [1,'KPIs'],
        [2,screen2,'KPIs – Activity Metrics',29.11],
        [2,screen3,'KPIs – Activity Metrics',29.11],
        [2,screen4,'KPIs – Activity Metrics',29.11],
        [2,screen5,'KPIs – Business Metrics',29.11],
        [2,screen6,'KPIs – Headcount',29.11],
        [1,'P&L'],
        [2,screen7,'P&L - Revenue and Cost of sales',22.59],
        [2,screen8,'P&L - Fixed costs',23.09],
        [2,screen9,'P&L - Other results',21.63],
        [1,'Cash'],
        [2,screen10,'Cash',24.47],   
        [2,screen11,"Cash",27.1]
        ]
    
    
    crear_ppt(template,lista_slides,guardar_como,mes)