# -*- coding: utf-8 -*-
"""
Created on Wed Aug 11 18:11:54 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def payflow(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING->"
    hoja2="P&L"
    hoja3="CF"
 

    lista=[
            [hoja2,4,4,16,58,"PL1"],
            [hoja2,19,4,33,58,"PL2"],
            [hoja2,35,4,43,58,"PL3"],
            [hoja2,4,4,5,58,"PL_titulos"],
            [hoja3,4,4,12,58,"CF1"],
            [hoja3,16,4,24,58,"CF2"],
            [hoja3,4,4,5,58,"CF_titulos"]
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[6],lista_imagenes2[5])
  



    
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[1]
    plot2=lista_imagenes1[2]

    screen2=lista_imagenes2[0]
    screen3=lista_imagenes2[1]
    screen4=lista_imagenes2[2]
    screen5=lista_imagenes2[4]
    screen6=lista_imagenes2[5]




    #eliminar imágenes auxiliares
    remove(lista_imagenes2[3])
    remove(lista_imagenes2[6])

    
    
    lista_slides=[
        [3,(screen1,plot1,plot2)],
        [1,"P&L"],
        [2,screen2,'P&L - Revenue and Cost of sales'],
        [2,screen3,'P&L - Fixed costs'],
        [2,screen4,'P&L - Other results'],
        [1,'Cash'],
        [2,screen5,'Operational Cash'],
        [2,screen6,'Financial Cash']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)