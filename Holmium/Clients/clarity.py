# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 18:09:30 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def clarity(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING->"
    hoja2="PL"
    hoja3="CF"
    hoja4="KPIs"

    lista=[
            [hoja2,1,3,25,82,"PL1"],
            [hoja2,28,3,47,82,"PL2"],
            [hoja2,49,3,57,82,"PL3"],
            [hoja2,1,3,5,82,"PL_titulos"],
            [hoja3,2,4,27,82,"CF1"],
            [hoja4,9,3,26,82,"KPIs1"],
            [hoja4,28,3,38,82,"KPIs2"],
            [hoja4,71,3,82,82,"KPIs3"],
            [hoja4,85,3,96,82,"KPIs4"],
            [hoja4,1,3,5,82,"KPIs_titulos"],
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    
    lista_imagenes1,lista_imagenes2,x=prints_and_plots(archivo,hoja1,lista,guardar_en)
    
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[5])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[6])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[8])

    
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[1]
    plot2=lista_imagenes1[2]

    screen2=lista_imagenes2[0]
    screen3=lista_imagenes2[1]
    screen4=lista_imagenes2[2]
    screen5=lista_imagenes2[4]
    screen6=lista_imagenes2[5]
    screen7=lista_imagenes2[6]
    screen8=lista_imagenes2[7]
    screen9=lista_imagenes2[8]

    #eliminar imágenes auxiliares
    remove(lista_imagenes2[3])
    remove(lista_imagenes2[9])
    
    lista_slides=[
        [3,(screen1,plot1,plot2)],
        [1,'KPIs'],
        [2,screen6,'KPIs – SAAS Metrics (MRR)'],
        [2,screen7,'KPIs – SAAS Metrics (Accounts)'],
        [2,screen8,'KPIs – Efficiency'],
        [2,screen9,'KPIs – Headcount'],
        [1,"P&L"],
        [2,screen2,'P&L - Revenue and Cost of sales (USD)'],
        [2,screen3,'P&L - Fixed costs (USD)'],
        [2,screen4,'P&L - Other results (USD)'],
        [1,'Cash'],
        [2,screen5,'Cash']
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)