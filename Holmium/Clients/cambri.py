# -*- coding: utf-8 -*-
"""
Created on Wed Aug 11 17:20:25 2021

@author: romina
"""

from Functions.funciones import juntar_imagenes, prints_and_plots,crear_ppt
from os.path import join,abspath
from os import pardir, remove

def cambri(archivo,guardar_como,template,mes):
    #Print y gráficos de slide2
    
    
    hoja1="REPORTING->"
    hoja2="PL"
    hoja3="CF"
    hoja4="KPIs-LIC"
    hoja5="Hiring"
    hoja6="KPIs-NON"
    lista=[
            [hoja2,4,4,23,58,"PL1"],
            [hoja2,25,4,42,58,"PL2"],
            [hoja2,44,4,52,58,"PL3"],
            [hoja2,4,4,5,58,"PL_titulos"],
            [hoja3,4,4,12,58,"CF1"],
            [hoja3,16,4,25,58,"CF2"],
            [hoja3,4,4,5,58,"CF_titulos"],
            [hoja4,9,4,13,58,"KPIs1"],
            [hoja4,60,4,79,58,"KPIs2"],
            [hoja4,4,4,5,58,"KPIs_titulos"],
            [hoja1,1,2,33,16,"REPORTING"],
            [hoja5,1,2,32,25,"Hiring"],
            [hoja1,1,2,4,16,"Reporting_titulo"],
            [hoja6,4,4,11,58,"KPIs-NON"]
        ]
    
    guardar_en=abspath(join(guardar_como, pardir))
    lista_imagenes1,lista_imagenes2,lista_imagenes3=prints_and_plots(archivo,hoja1,lista,guardar_en,hoja_resumen2="Hiring")
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[1])
    juntar_imagenes(lista_imagenes2[3],lista_imagenes2[2])
    juntar_imagenes(lista_imagenes2[6],lista_imagenes2[5])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[7])
    juntar_imagenes(lista_imagenes2[9],lista_imagenes2[8])
    


    
    screen1=lista_imagenes1[0]
    plot1=lista_imagenes1[1]
    plot2=lista_imagenes1[3]
    

    screen2=lista_imagenes2[0]
    screen3=lista_imagenes2[1]
    screen4=lista_imagenes2[2]
    screen5=lista_imagenes2[4]
    screen6=lista_imagenes2[5]
    screen7=lista_imagenes2[7]
    screen8=lista_imagenes2[8]
    screen9=lista_imagenes2[11]
    screen10=lista_imagenes2[12]
    screen11=lista_imagenes2[13]


    #eliminar imágenes auxiliares
    remove(lista_imagenes2[3])
    remove(lista_imagenes2[6])
    remove(lista_imagenes2[9])
    
    
    lista_slides=[
        [5,screen1,3.43,0.82,26.41],
        [5,(screen10,plot1,plot2),(3.43,3.43,16.93),(1.12,4.32,4.32),(26.86,13.02,13.68)],
        [5,screen9,1.72,1.82,30.03],
        [1,'KPIs'],
        [2,screen7,'KPIs – License (LIC)',28.51],
        [2,screen8,'KPIs - Efficiency (LIC)',28.6],
        [2,screen11,"KPIs – Non licensed (NON)",30.61],
        [1,"P&L"],
        [2,screen2,'P&L - Revenue and Cost of sales',26.93],
        [2,screen3,'P&L - Fixed costs',24.04],
        [2,screen4,'P&L - Other results',30.4],
        [1,'Cash'],
        [2,screen5,'Operational Cash',30.67],
        [2,screen6,'Financial Cash',27.92]
        ]
    
    crear_ppt(template,lista_slides,guardar_como,mes)