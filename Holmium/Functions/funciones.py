# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 18:34:46 2021

@author: romina
"""

from win32com.client import Dispatch
from PIL import ImageGrab,Image
from os import walk,makedirs
from os.path import join
from pptx import Presentation 
from pptx.util import Cm
import win32clipboard
from time import sleep

def move_slide( presentation, old_index, new_index):
    xml_slides = presentation.slides._sldIdLst  # pylint: disable=W0212
    slides = list(xml_slides)
    xml_slides.remove(slides[old_index])
    xml_slides.insert(new_index, slides[old_index])

def juntar_imagenes(imagen1,imagen2):
    img1=Image.open(imagen1)
    img2=Image.open(imagen2)
    x1,y1=img1.size
    x2,y2=img2.size
    x=x1
    if x1>x2:
        img1.resize(x2,y1)
        x=x2
    elif x2>x1:
        img2.resize(x1,y2)
        x=x1
        
    #creating a new image empty:
    y=y1+y2
    img3=Image.new("RGB",(x,y),"white")
    
    #pasting first image
    img3.paste(img1,(0,0))
    #pasting second image
    img3.paste(img2,(0,y1))    
    img3.save(imagen2)
    img1.close()
    img2.close()
    img3.close()

def join_image_h(im1_path, im2_path, sep = 0.0):
    # Load images
    im1=Image.open(im1_path)
    im2=Image.open(im2_path)
    # Join horizontally
    im3_width, im3_height = int((1.0 + sep) * (im1.width + im2.width)), max(im1.height, im2.height)
    im3 = Image.new('RGB', (im3_width, im3_height), "white")
    im3.paste(im1, (0, 0))
    im3.paste(im2, (im1.width + int(sep * (im1.width + im2.width)), 0))
    # Close and save new image
    im1.close()
    im2.close()
    im3.save(im2_path)
    im3.close()
    return


    
def save_plots(archivo,hoja,guardar_en):
    img_list1=[]
    excel = Dispatch('Excel.Application')
    excel.Visible = False
    excel.DisplayAlerts = False
    
    wb = excel.Workbooks.Open(archivo)
    ws = wb.Worksheets[hoja]
    carpeta_imagenes=guardar_en + "\\images"
    subdirectorios=[x[0] for x in walk(guardar_en)]
    if carpeta_imagenes not in subdirectorios: makedirs(carpeta_imagenes)
    i=0
    for n, shape in enumerate(ws.Shapes):
            i=i+1
            
            image_name="plot_" + str(i) +".png"
            imgFile = join(carpeta_imagenes,image_name)
            try:
                shape.Copy()
                image = ImageGrab.grabclipboard()
                image.save(imgFile) # General This code is easy to make mistakes, because the clipboard may have no content, it is best to add try...except...
            except:
                pass
            img_list1.append(imgFile)
            pass
        
    pass
    image.close()
    wb.Close()
    excel.Visible = True
    excel.DisplayAlerts = True
    
    excel.Quit()
    
    return img_list1
    

def screenshots(archivo,lista_hojas_rangos,guardar_en):
    
    excel = Dispatch('Excel.Application')
    excel.Visible = False
    excel.DisplayAlerts = False
    
    img_list1=[]
    wb = excel.Workbooks.Open(archivo)
    carpeta_imagenes=guardar_en + "\\images"
    subdirectorios=[x[0] for x in walk(guardar_en)]
    if carpeta_imagenes not in subdirectorios: makedirs(carpeta_imagenes)
    for linea in lista_hojas_rangos:
        hoja=linea[0]
        rango=linea[1]
        ws = wb.Worksheets[hoja]
        if type(rango)==tuple:
            for i in rango:
                guardar_como=i
                try:
                    ws.Range(i).CopyPicture(Format=2)  
                    img = ImageGrab.grabclipboard()
                    imgFile = join(carpeta_imagenes,guardar_como)
                    img.save(imgFile)
                    img_list1.append(imgFile)
                    img.close()
                except:
                    pass
    return img_list1
    
def prints_and_plots(archivo,hoja_resumen,lista_part_prints,guardar_en,hoja_resumen2=None, resumen_op = True):
    """
    

    Parameters
    ----------
    archivo : ruta del archivo
    hoja_resumen : string
    hoja_PL : string
    hoja_CF : string
    hoja_KPIS : string -- nombre de la hoja 
    
    lista_part_prints: lista que contiene la hoja,las coordenadas de lo que se quiere hacer print y el nombre con el que se quiere guardar:
                       Ej:  [
                            [hoja1,1,1,34,70,"KPIs"]
                            [hoja2,1,1,54,70,"PL1"]
                            ]

    Returns
    -------
    img_list : TYPE
        DESCRIPTION.
        
        
    Esta función es la unión de screenshot_all y screenshot_part para evitar abrir y cerrar el archivo excel por cada screen

    """
    #win32clipboard.OpenClipboard()
    
    img_list1=[]
    excel = Dispatch('Excel.Application')
    excel.Visible = False
    excel.DisplayAlerts = False
    
    wb = excel.Workbooks.Open(archivo)
    ws = wb.Worksheets[hoja_resumen]
    ultima_fila=ws.UsedRange.Rows.Count
    ultima_col=ws.UsedRange.Columns.Count
    carpeta_imagenes=guardar_en + "\\images"
    subdirectorios=[x[0] for x in walk(guardar_en)]
    if carpeta_imagenes not in subdirectorios: makedirs(carpeta_imagenes)
    guardar_como= "REPORTING.jpg"
    ws.Activate()

    if resumen_op:
        try:
            
            ws.Range(ws.Cells(1,1),ws.Cells(ultima_fila,ultima_col)).CopyPicture(Format=2)  
            img = ImageGrab.grabclipboard()
            imgFile = join(carpeta_imagenes,guardar_como)
            img.save(imgFile)
            img_list1.append(imgFile)
            img.close()
        except:
            pass
        
        i=0
        for n, shape in enumerate(ws.Shapes):
                i=i+1
                
                image_name="plot_" + str(i) +".png"
                imgFile = join(carpeta_imagenes,image_name)
                try:
                    shape.Copy()
                    image = ImageGrab.grabclipboard()
                    image.save(imgFile) # General This code is easy to make mistakes, because the clipboard may have no content, it is best to add try...except...
                    img_list1.append(imgFile)
                    image.close()  
                
                except:
                    pass
                
                pass
        
        pass
    else:
        pass
    
    
    img_list2=[]
    for linea in lista_part_prints:
        ws = wb.Worksheets[linea[0]]
        ws.Activate()
        guardar_como=linea[5] + ".jpg"
        sleep(0.5)
        ws.Range(ws.Cells(linea[1],linea[2]),ws.Cells(linea[3],linea[4])).CopyPicture(Format=2)  
        img = ImageGrab.grabclipboard()
        imgFile = join(carpeta_imagenes,guardar_como)
        img_list2.append(imgFile)
        img.save(imgFile)
        img.close()
    
    img_list3=[]
    if hoja_resumen2!=None:
        
        ws = wb.Worksheets[hoja_resumen2]
        
        for n, shape in enumerate(ws.Shapes):
                i=i+1
                image_name="plot_" + str(i) +".png"
                imgFile = join(carpeta_imagenes,image_name)
                try:
                    shape.Copy()
                    image = ImageGrab.grabclipboard()
                    image.save(imgFile) # General This code is easy to make mistakes, because the clipboard may have no content, it is best to add try...except...
                except:
                    pass
                img_list3.append(imgFile)
                pass
        
    wb.Close(False)
    wb=None
    excel.Visible = True
    excel.DisplayAlerts = True
    excel.Quit()
    excel=None
   # win32clipboard.CloseClipboard()
    
    
    return img_list1,img_list2,img_list3

def copy_img_adapted(prs, shapes, img_path, h_target_ratio, w_target_ratio, top_space_ratio, loc = 'center'):
    # Extract image dimensions ratio
    w_img, h_img = Image.open(img_path).size
    img_ratio = h_img/ w_img

    # ideal image dimensions ratio and location
    if loc == 'left' or loc == 'right':
        target_ratio = (h_target_ratio * prs.slide_height) / (w_target_ratio * prs.slide_width/2)
    else:
        target_ratio = (h_target_ratio * prs.slide_height) / (w_target_ratio * prs.slide_width)

    # Depending on image ratio, image in ppt is defined by width or height
    # if img_ratio > target_ratio -> height rules
    # if img_ratio < target_ratio -> width rules
    if img_ratio > target_ratio: # -> height rules
        ppt_h = h_target_ratio * prs.slide_height
        ppt_w = 1.0/img_ratio * ppt_h

        # Defining location: 'center', 'left', 'right'
        if loc == 'left':
            x_loc = 0.25 * prs.slide_width - 0.5 * ppt_w
        elif loc == 'right':
            x_loc = 0.75 * prs.slide_width - 0.5 * ppt_w
        else:
            x_loc = 0.5 * prs.slide_width - 0.5 * ppt_w

        shapes.add_picture(img_path, 
                    left= x_loc, 
                    top = (top_space_ratio + h_target_ratio/2) * prs.slide_height - 0.5 * ppt_h, 
                    height= ppt_h)

    else: # -> width rules
        ppt_w = w_target_ratio * prs.slide_width
        ppt_h = img_ratio * ppt_w

         # Defining location: 'center', 'left', 'right'
        if loc == 'left':
            x_loc = 0.25 * prs.slide_width - 0.5 * ppt_w
        elif loc == 'right':
            x_loc = 0.75 * prs.slide_width - 0.5 * ppt_w
        else:
            x_loc = 0.5 * prs.slide_width - 0.5 * ppt_w

        shapes.add_picture(img_path, 
                    left= x_loc, 
                    top = (top_space_ratio + h_target_ratio/2) * prs.slide_height - 0.5 * ppt_h, 
                    width= ppt_w)
    return


def crear_ppt(template,lista,guardar_como,mes):
    """
    

    Parameters
    ----------
    template : url del template
    layout_types= titulos=1, resumen=2, blancos=3
    lista : lista con información sobre cada slide que se quiere añadir:
            [ [layout_type,(screen1,plot1,...),text=""],...
                ]
            
    5 tipos de componentes en la lista:
            de layout_type=1 --> [1,text="TITULO"]
            de layout_type=2 --> [2,(screen2),text="TITULO",largo]
            de layout_type=3 --> [3,(screen1,plot1,plot2)]
            de layout_type=4 --> [4,screen4] solo una imagen y nada más, sin texto
            de layout_type=5 --> [5,(screen1,screen2,screen3),(pos_x1,pos_x2,pos_x3),(y1,y2,y3),(largo1,largo2,largo3)]
            de layout_type=6 --> [6, text, (screen1,screen2,screen3),(pos_x1,pos_x2,pos_x3),(y1,y2,y3),(largo1,largo2,largo3)]
    guardar_como : TYPE
        DESCRIPTION.
    
    Returns
    -------
    None.

    """
    
    prs  = Presentation(template)
    layout_title = prs.slide_layouts[1]
    layout_resumen = prs.slide_layouts[2]
    layout_empty = prs.slide_layouts[3] 
    
    slides = prs.slides
    slide_titulo=slides[0]
    title_placeholder = slide_titulo.shapes.title
    titulo="Reporting " + str(mes)
    title_placeholder.text=titulo
    
    for linea in lista:
        if linea[0]==1:
            #Crear slide con título
            slide_pl_title=prs.slides.add_slide(layout_title)
            title_placeholder = slide_pl_title.shapes.title
            title_placeholder.text = linea[1]
        elif linea[0]==2:
            #Crear slide con resumen
            slide_pl=prs.slides.add_slide(layout_resumen)
            title_placeholder = slide_pl.shapes.title
            title_placeholder.text = linea[2]
            
            shapes=slide_pl.shapes

            if len(linea)==4:
                w=Cm(linea[3])
                x=Cm(16.93-linea[3]/2)
                shapes.add_picture(linea[1], left = x, top = prs.slide_height * 0.20, width = w)
            else:
                copy_img_adapted(prs, shapes, linea[1], 0.5, 0.8, 0.2)

            
        elif linea[0]==3:
            new_slide=prs.slides.add_slide(layout_empty)
            shapes = new_slide.shapes
            try:
                shapes.add_picture(linea[1][2],left=prs.slide_width/2,top=prs.slide_height/2,height=prs.slide_height/2,width=prs.slide_width/2)
                shapes.add_picture(linea[1][0],left=0,top=0,height=prs.slide_height/2,width=prs.slide_width)
                shapes.add_picture(linea[1][1],left=0,top=prs.slide_height/2,height=prs.slide_height/2,width=prs.slide_width/2)
                
            except:
                shapes.add_picture(linea[1][0],left=0,top=0,height=prs.slide_height/2,width=prs.slide_width)
                shapes.add_picture(linea[1][1],left=0,top=prs.slide_height/2,height=prs.slide_height/2,width=prs.slide_width)
            
        elif linea[0]==4:
            new_slide=prs.slides.add_slide(layout_empty)
            shapes = new_slide.shapes
            copy_img_adapted(prs, shapes, linea[1], 0.95, 0.95, 0.025)

        elif linea[0]==5:
            new_slide=prs.slides.add_slide(layout_empty)
            shapes = new_slide.shapes
            if type(linea[1])==tuple:
                for i in range(0,len(linea[1])):
                    x=Cm(linea[2][i])
                    y=Cm(linea[3][i])
                    largo=Cm(linea[4][i])
                    shapes.add_picture(linea[1][i], left=x, top=y, width=largo)
            else:
                x=Cm(linea[2])
                y=Cm(linea[3])
                largo=Cm(linea[4])
                shapes.add_picture(linea[1], left=x, top=y, width=largo)
            

        elif linea[0]==6:
            #Crear slide con resumen
            new_slide=prs.slides.add_slide(layout_resumen)
            title_placeholder = new_slide.shapes.title
            title_placeholder.text = linea[1]  

            # add images
            shapes = new_slide.shapes
            if type(linea[2])==tuple:
                for i in range(0,len(linea[2])):
                    x=Cm(linea[3][i])
                    y=Cm(linea[4][i])
                    largo=Cm(linea[5][i])
                    shapes.add_picture(linea[2][i],left=x,top=y,width=largo)
            else:
                x=Cm(linea[3])
                y=Cm(linea[4])
                largo=Cm(linea[5])
                shapes.add_picture(linea[2],left=x,top=y,width=largo)

        elif linea[0]==7:
            # Slide resumen with title and picture at one side
            slide_pl=prs.slides.add_slide(layout_resumen)
            title_placeholder = slide_pl.shapes.title
            title_placeholder.text = linea[3]
            
            shapes=slide_pl.shapes

            copy_img_adapted(prs, shapes, linea[1], 0.75, 0.8, 0.2, linea[2])

        elif linea[0] == 8:
            #Crear custom slide f(8, slide_id, image_ids, h_ratio, w_ratio, top_space, text)
            # Select slide 
            slide_pl=prs.slides.add_slide(prs.slide_layouts[linea[1]])
            #
            if linea[6] != "":
                title_placeholder = slide_pl.shapes.title
                title_placeholder.text = linea[6]
            
            for i in range(len(linea[2])):
                shapes=slide_pl.shapes
                # = f(ppt, shapes, image_path, h_ratio, w_ratio, top_space)
                copy_img_adapted(prs, shapes, linea[2][i], linea[3][i], linea[4][i], linea[5][i])


    #Mover slide con logos al final
    a=len(slides)-1
    move_slide(prs,1,a)
    
    
    prs.save(guardar_como)
    

def prints(archivo,lista_part_prints,guardar_en):
    """
    

    Parameters
    ----------
    archivo : ruta del archivo
    hoja_resumen : string
    hoja_PL : string
    hoja_CF : string
    hoja_KPIS : string -- nombre de la hoja 
    
    lista_part_prints: lista que contiene la hoja,las coordenadas de lo que se quiere hacer print y el nombre con el que se quiere guardar:
                       Ej:  [
                            [hoja1,1,1,34,70,"KPIs"]
                            [hoja2,1,1,54,70,"PL1"]
                            ]

    Returns
    -------
    img_list : TYPE
        DESCRIPTION.
        
        
    Esta función es la unión de  screenshot_part para evitar abrir y cerrar el archivo excel por cada screen

    """
    
    excel = Dispatch('Excel.Application')
    excel.Visible = False
    excel.DisplayAlerts = False
    
    wb = excel.Workbooks.Open(archivo)
    
    carpeta_imagenes=guardar_en  +"\\"+ "images"
    
    subdirectorios=[x[0] for x in  walk(guardar_en)]
    if carpeta_imagenes not in subdirectorios: makedirs(carpeta_imagenes)
    
    
    
    img_list2=[]
    for linea in lista_part_prints:
        ws = wb.Worksheets[linea[0]]
        guardar_como=linea[5] + ".jpg"
        sleep(0.5)
        ws.Range(ws.Cells(linea[1],linea[2]),ws.Cells(linea[3],linea[4])).CopyPicture(Format=2)  
        img = ImageGrab.grabclipboard()
        imgFile = join(carpeta_imagenes,guardar_como)
        img_list2.append(imgFile)
        img.save(imgFile)
        img.close()
    
    
    wb.Close(False)
    excel.Visible = True
    excel.DisplayAlerts = True
    wb=None
    excel.Quit()
    excel=None
    return img_list2


def prints_and_plots_cell_names(archivo,hoja_resumen,lista_part_prints,guardar_en,hoja_resumen2=None):
    """
    Es la misma que prints and plots pero para cuando se ponen nombres a las secciones que se quieren añadir al ppt

    Parameters
    ----------
    archivo : ruta del archivo
    hoja_resumen : string
    hoja_PL : string
    hoja_CF : string
    hoja_KPIS : string -- nombre de la hoja 
    
    lista_part_prints: lista que contiene la hoja,las coordenadas de lo que se quiere hacer print y el nombre con el que se quiere guardar:
                       Ej:  [
                            [hoja1,1,1,34,70,"KPIs"]
                            [hoja2,1,1,54,70,"PL1"]
                            ]

    Returns
    -------
    img_list : TYPE
        DESCRIPTION.
        
        
    Esta función es la unión de screenshot_all y screenshot_part para evitar abrir y cerrar el archivo excel por cada screen

    """
    #win32clipboard.OpenClipboard()
    
    img_list1=[]
    excel = Dispatch('Excel.Application')
    excel.Visible = True
    excel.DisplayAlerts = False
    
    wb = excel.Workbooks.Open(archivo)
    ws = wb.Worksheets[hoja_resumen]
    ultima_fila=ws.UsedRange.Rows.Count
    ultima_col=ws.UsedRange.Columns.Count
    carpeta_imagenes=guardar_en + "\\images"
    subdirectorios=[x[0] for x in walk(guardar_en)]
    if carpeta_imagenes not in subdirectorios: makedirs(carpeta_imagenes)
    guardar_como= "REPORTING.jpg"
    ws.Activate()
    try:
        
        ws.Range(ws.Cells(1,1),ws.Cells(ultima_fila,ultima_col)).CopyPicture(Format=2)  
        img = ImageGrab.grabclipboard()
        imgFile = join(carpeta_imagenes,guardar_como)
        img.save(imgFile)
        img_list1.append(imgFile)
        img.close()
    except:
        pass
    
    i=0
    for n, shape in enumerate(ws.Shapes):
            i=i+1
            
            image_name="plot_" + str(i) +".png"
            imgFile = join(carpeta_imagenes,image_name)
            try:
                shape.Copy()
                image = ImageGrab.grabclipboard()
                image.save(imgFile) # General This code is easy to make mistakes, because the clipboard may have no content, it is best to add try...except...
                img_list1.append(imgFile)
                image.close()  
            
            except:
                pass
            
            pass
      
    pass
    
    
    img_list2=[]
    for linea in lista_part_prints:
        ws = wb.Worksheets[linea[0]]
        ws.Activate()
        guardar_como=linea[1] + ".jpg"
        sleep(0.5)
        ws.Range(linea[1]).CopyPicture(Format=2)  
        img = ImageGrab.grabclipboard()
        imgFile = join(carpeta_imagenes,guardar_como)
        img_list2.append(imgFile)
        img.save(imgFile)
        img.close()
    
    img_list3=[]
    if hoja_resumen2!=None:
        
        ws = wb.Worksheets[hoja_resumen2]
        
        for n, shape in enumerate(ws.Shapes):
                i=i+1
                image_name="plot_" + str(i) +".png"
                imgFile = join(carpeta_imagenes,image_name)
                try:
                    sleep(0.5)
                    shape.Copy()
                    image = ImageGrab.grabclipboard()
                    image.save(imgFile) # General This code is easy to make mistakes, because the clipboard may have no content, it is best to add try...except...
                except:
                    pass
                img_list3.append(imgFile)
                pass
        
    wb.Close(False)
    excel.Visible = True
    excel.DisplayAlerts = True
    wb=None
   # win32clipboard.CloseClipboard()
    excel.Quit()
    excel=None
    
    return img_list1,img_list2,img_list3