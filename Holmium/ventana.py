# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 15:01:17 2021

@author: romina
"""

import PySimpleGUI as sg
from datetime import datetime
from dateutil.relativedelta import relativedelta
from time import time as time2
from getpass import getuser

import ctypes
myappid = 'mycompany.myproduct.subproduct.version' # arbitrary string
ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)


clientes=["Gelt","Cobee","uSizy","Nora","Idoven","Gasmobi","Validated ID","Graphext",
          "Plantarse","DotGIS","Clarity","Agrosingularity","Nova","Nubentos",
          "Build38","Camelina","Ifeel","Circular","Cambri","Payflow","Iomob",
          "Gelt Argentina","Gelt Brasil","Gelt Mexico","Gelt Tech","Gelt Giro",
          "Prosegur Crypto","Innitius", "Baia", "Tenzir", "Liux", "Ekuore", 
          "MediQuo", "Doxel"]

clientes=sorted(clientes, key=lambda v: v.upper())
meses=["January","February","March","April","May","June","July","August","September"
       ,"October","November","December"]

def main():
    sg.theme("SystemDefaultForReal") # ("BlueMono") # 
    sg.set_options(font=('Helvetica', 10))
    layout1 = [[sg.T("")],
          [sg.Text("                    Client:"),sg.Combo(clientes,key="cliente")],
          [sg.Text("                   Month:"),sg.Combo(meses,default_value= (datetime.now() + relativedelta(months=-1)).strftime("%B"), key="meses")],
          [sg.T("")], [sg.Text("                   Report:"), sg.Input(), sg.FileBrowse(key="archivo")],
                      [sg.Text("               Template:"), sg.Input(), sg.FileBrowse(key="template")],
          [sg.T("")],
          [sg.T("")],[sg.T(' Save in:        ', pad=(0,10)), sg.Radio('default (Desktop)', "RADIO2", default=True, key="-R10-"),
            sg.T(' o '), sg.FolderBrowse("Carpeta", key="-IN3-", target=("-IN4-")) , sg.Input(key="-IN4-", size=(34,1))],
            [sg.T('Save as:      '), sg.InputText(size=(65,1), key="new_name")],
          [sg.T("                                   ",pad=(40,50,100,100)), sg.Button("Submit",key="submit1"), sg.T("  "), sg.Button("Cancel")]
             ]
    win1=sg.Window("Holmium",layout1,finalize=True,icon="helmio.ico",auto_size_text=18)  
    while True:
        event1, values1 = win1.read()
        if event1 == sg.WIN_CLOSED:
            break
        elif event1 == "Cancel":
            win1.close()
            break
        
        elif event1=="submit1":
            
            cliente=values1["cliente"]
            
            archivo=values1["archivo"]
            template=values1["template"]
            mes=values1["meses"]
            
            guardar_como=values1["new_name"]
            
            suffix=".pptx"   
            
            
            if (values1["-IN4-"])!="":
                guardar_en=values1["-IN4-"]+"/"
                
            elif values1["-IN4-"]=="":
                user_path=getuser()
                guardar_en=r"c:/Users/"+user_path+"/Desktop/"
            
            guardar_como=guardar_en+guardar_como+suffix
            
            t0=time2()
            if mes=="":
                sg.Popup("Choose a month")
            elif cliente=="Cobee":
                try:
                    from Clients.cobee import cobee
                    cobee(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
                    
            elif cliente=="Gelt":
                try:
                    from Clients.gelt import gelt
                    gelt(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="uSizy":
                try:
                    from Clients.usizy import usizy
                    usizy(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Nora":
                try:
                    from Clients.nora import nora
                    nora(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Idoven":
                try:
                    from Clients.idoven import idoven
                    idoven(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Gasmobi":
                try:
                    from Clients.gasmobi import gasmobi
                    gasmobi(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Validated ID":
                # try:
                    from Clients.validated_id import validated_id
                    validated_id(archivo, guardar_como, template,mes)
                    t1 = time2()
                    tiempo = "Time: " + str(round(t1 - t0, 2)) + " sec."
                    sg.Popup("Done!", tiempo)
                # except:
                #     sg.Popup("ERROR")
            elif cliente=="Graphext":
                try:
                    from Clients.graphext import graphext
                    graphext(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Plantarse":
                try:
                    from Clients.plantarse import plantarse
                    plantarse(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("PowerPoint creado",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="DotGIS":
                try:
                    from Clients.dotgis import dotgis
                    dotgis(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Clarity":
                try:
                    from Clients.clarity import clarity
                    clarity(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Agrosingularity":
                try:
                    from Clients.agrosingularity import agrosingularity
                    agrosingularity(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("PowerPoint creado",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Nova":
                try:
                    from Clients.nova import nova
                    nova(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Nubentos":
                try:
                    from Clients.nubentos import nubentos
                    nubentos(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Build38":
                try:
                    from Clients.build38 import build
                    build(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Camelina":
                try:
                    from Clients.camelina import camelina
                    camelina(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Ifeel":
                try:
                    from Clients.ifeel import ifeel
                    ifeel(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Cobbler Union":
                try:
                    from Clients.cobbler import cobbler
                    cobbler(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Circular":
                try:
                    from Clients.circular import circular
                    circular(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Cambri":
                try:
                    from Clients.cambri import cambri
                    cambri(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Tiempo: " +str(round(t1-t0,2)) + " seguntos"
                    sg.Popup("PowerPoint creado",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Payflow":
                try:
                    from Clients.payflow import payflow
                    payflow(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Iomob":
                try:
                    from Clients.iomob import iomob
                    iomob(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Gelt Argentina":
                try:
                    from Clients.gelt_arg import gelt_arg
                    gelt_arg(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Gelt Brasil":
                try:
                    from Clients.gelt_br import gelt_br
                    gelt_br(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Gelt Mexico":
                try:
                    from Clients.gelt_mx import gelt_mx
                    gelt_mx(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Gelt Tech":
                # try:
                    from Clients.gelt_tech import gelt_tech
                    gelt_tech(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                # except:
                #     sg.Popup("ERROR")
            elif cliente=="Gelt Giro":
                # try:
                    from Clients.gelt_giro import gelt_giro
                    gelt_giro(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                # except:
                #     sg.Popup("ERROR")
            elif cliente=="Prosegur Crypto":
                # try:
                    from Clients.prosegur_crypto import prosegur_crypto
                    prosegur_crypto(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                # except:
                #     sg.Popup("ERROR")
            elif cliente=="Innitius":
                try:
                    from Clients.innitius import innitius
                    innitius(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Baia":
                try:
                    from Clients.baia import baia
                    baia(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Tenzir":
                try:
                    from Clients.tenzir import tenzir
                    tenzir(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Liux":
                try:
                    from Clients.liux import liux
                    liux(archivo,guardar_como,template,mes)
                    t1=time2()
                    tiempo="Time: " +str(round(t1-t0,2)) + " sec."
                    sg.Popup("Done!",tiempo)
                except:
                    sg.Popup("ERROR")
            elif cliente=="Ekuore":
                try:
                    from Clients.ekuore import ekuore
                    ekuore(archivo, guardar_como, template,mes)
                    t1 = time2()
                    tiempo = "Time: " + str(round(t1 - t0, 2)) + " sec."
                    sg.Popup("Done!", tiempo)
                except:
                    sg.Popup("ERROR")
            
            elif cliente == "MediQuo":
                try:
                    from Clients.mediquo import mediquo
                    mediquo(archivo, guardar_como, template,mes)
                    t1 = time2()
                    tiempo = "Time: " + str(round(t1 - t0, 2)) + " sec."
                    sg.Popup("Done!", tiempo)
                except:
                    sg.Popup("ERROR")
                    
            elif cliente == "Doxel":
                try:
                    from Clients.doxel import doxel
                    doxel(archivo, guardar_como, template,mes)
                    t1 = time2()
                    tiempo = "Time: " + str(round(t1 - t0, 2)) + " sec."
                    sg.Popup("Done!", tiempo)
                except:
                    sg.Popup("ERROR")
                    
    win1.close()

if __name__=="__main__":
    main()